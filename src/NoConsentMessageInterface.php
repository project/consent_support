<?php

namespace Drupal\consent_support;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Url;

/**
 * Interface NoConsentMessageInterface.
 *
 * Describes the No-Consent Message service functions.
 */
interface NoConsentMessageInterface extends CacheableDependencyInterface {

  /**
   * Get the no-consent message 'context' class.
   *
   * This value is derived from the 'context' URL query parameter by default.
   * When no context query parameter is present in current request, the value
   * will default to 'default'.
   *
   * Using the $context parameter, any desired CSS class can be generated,
   * regardless the derived context value.
   *
   * @param string|null $context
   *   An override context value to create the CSS class with.
   *
   * @return string
   *   The CSS class.
   */
  public function getContextCssClass(string $context = NULL): string;

  /**
   * Get no-consent message for iframes with given category.
   *
   * @param string $category_key
   *   The category ID key.
   * @param string|null $context
   *   A context value to pass to the no-consent message template.
   * @param string|null $langcode
   *   The desired translation langcode. Defaults to current interface language.
   *
   * @return array
   *   The render array.
   */
  public function getMessageForIframe(string $category_key, string $context = NULL, string $langcode = NULL): array;

  /**
   * Get no-consent message for scripts with given category.
   *
   * @param string $category_key
   *   The category ID key.
   * @param string|null $context
   *   A context value to pass to the no-consent message template.
   * @param string|null $langcode
   *   The desired translation langcode. Defaults to current interface language.
   *
   * @return array
   *   The render array.
   */
  public function getMessageForScript(string $category_key, string $context = NULL, string $langcode = NULL): array;

  /**
   * Get classes for script no-consent message wrapper.
   *
   * These class(es) will be used to remove the messages when consent is given.
   *
   * @param string $category_key
   *   The category ID key.
   *
   * @return string
   *   The class string.
   *
   * @see /js/cookiepro.js
   */
  public function getMessageWrapperClassForScript(string $category_key): string;

  /**
   * Get No-Consent message for given category ID key.
   *
   * @param string $category_key
   *   The category ID key.
   * @param string|null $context
   *   A context parameter to add to the url. This will be passed on to the
   *   no-consent page and message template.
   * @param bool $absolute
   *   Whether to return an absolute URL. Defaults to TRUE.
   * @param string|null $langcode
   *   The langcode. Defaults to the current language code for the language type
   *   LanguageInterface::TYPE_URL.
   *
   * @return \Drupal\Core\Url
   *   The url.
   */
  public function getNoConsentPageUrl(string $category_key, string $context = NULL, bool $absolute = TRUE, string $langcode = NULL): Url;

}
