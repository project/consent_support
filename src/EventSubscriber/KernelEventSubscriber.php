<?php

namespace Drupal\consent_support\EventSubscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class KernelEventSubscriber.
 *
 * Adds X-Robots-Tag with 'noindex, nofollow' to the consent_support.no_consent
 * route response.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    // Run before HtmlResponseSubscriber::onRespond(), which has priority 0.
    $events[KernelEvents::RESPONSE][] = ['onResponse', 50];

    return $events;
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   */
  public function __construct(protected RouteMatchInterface $routeMatch) {}

  /**
   * Add 'noindex, nofollow' to the no-consent pages.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onResponse(ResponseEvent $event): void {

    if ($this->routeMatch->getRouteName() == 'consent_support.no_consent') {
      $response = $event->getResponse();
      $response->headers->set('X-Robots-Tag', ['noindex, nofollow']);
    }
  }

}
