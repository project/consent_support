<?php

namespace Drupal\consent_support\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Defines an interface for Consent Support entity plugin plugins.
 */
interface ConsentSupportPluginInterface extends PluginInspectionInterface {

  /**
   * Return locations to attach submit handlers to entities.
   *
   * This should return an array of arrays, e.g.:
   * [
   *   ['actions', 'submit', '#publish'],
   *   ['actions', 'publish', '#submit'],
   * ].
   */
  public function getFormSubmitHandlerAttachLocations(): array;

  /**
   * Alters render array for given entity to support CookiePro.
   *
   * @param array $build
   *   The render array.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display.
   * @param \Drupal\consent_support\Entity\ConfigInterface $config
   *   The Consent Support Config object for the entity.
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void;

}
