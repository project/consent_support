<?php

namespace Drupal\consent_support\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Consent Support plugin manager.
 */
class ConsentSupportPluginManager extends DefaultPluginManager {

  /**
   * Constructor for ConsentSupportPluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, protected EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct('Plugin/ConsentSupportPlugin', $namespaces, $module_handler, 'Drupal\consent_support\Plugin\ConsentSupportPluginInterface', 'Drupal\consent_support\Annotation\ConsentSupportPlugin');
    $this->alterInfo('consent_support_entity_plugin_info');
    $this->setCacheBackend($cache_backend, 'consent_support_entity_plugin_plugins');
  }

  /**
   * Create an instance of the first plugin found with string ID $entity_type.
   *
   * Create an instance of the first plugin found supporting the entity type
   * with string ID $entity_type.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   *
   * @return \Drupal\consent_support\Plugin\ConsentSupportPluginInterface
   *   The plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createInstanceByEntityType(string $entity_type): ConsentSupportPluginInterface {
    $plugin_ids = array_keys($this->loadDefinitionsByEntityType($entity_type));

    $plugin_id = (string) reset($plugin_ids);
    /** @var \Drupal\consent_support\Plugin\ConsentSupportPluginInterface $plugin */
    $plugin = $this->createInstance($plugin_id);
    return $plugin;
  }

  /**
   * Load plugins implementing entity with ID $entity_type.
   *
   * @param string $entity_type
   *   The string ID of the entity type.
   *
   * @return string[]
   *   An array of plugin definitions for the entity type with ID $entity_type.
   */
  public function loadDefinitionsByEntityType(string $entity_type): array {
    return array_filter($this->getDefinitions(), function ($var) use ($entity_type) {
      return $var['entityType'] === $entity_type;
    });
  }

  /**
   * Load the plugin definitions for the supported entity types.
   *
   * @return array
   *   An array of plugin definitions keyed by the entity type ID.
   */
  public function loadSupportedEntityTypePluginDefinitions(): array {

    $definitions = [];
    $all_definitions = $this->getDefinitions();

    // Filter out config entities.
    foreach ($all_definitions as $definition) {

      try {

        $entity_type = $this->entityTypeManager->getStorage($definition['entityType'])->getEntityType();
        if (!$entity_type instanceof ConfigEntityTypeInterface) {
          $definitions[$definition['entityType']] = $definition;
        }
      }
      catch (\Exception $exception) {
        // Silent fail.
      }
    }

    return $definitions;
  }

  /**
   * Load the IDs for the supported entity types.
   *
   * @return array
   *   An array of entity type IDs.
   */
  public function loadSupportedEntityTypeIds(): array {
    return array_keys($this->loadSupportedEntityTypePluginDefinitions());
  }

  /**
   * Load the string IDs for the supported bundle entity types.
   *
   * @return string[]
   *   An array of entity type ID strings.
   */
  public function loadSupportedBundleEntityTypeIds(): array {

    $ids = [];
    $all_definitions = $this->getDefinitions();

    // Get the bundle entity types corresponding with supported entity types.
    foreach ($all_definitions as $definition) {

      try {

        $bundle_entity_type = $this->entityTypeManager->getStorage($definition['entityType'])->getEntityType()->getBundleEntityType();
        if (!is_null($bundle_entity_type)) {

          $ids[] = $bundle_entity_type;
        }
      }
      catch (\Exception $exception) {
        // Silent fail.
      }
    }

    return $ids;
  }

}
