<?php

namespace Drupal\consent_support\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\consent_support\Config\ConfigKeysInterface;
use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\consent_support\Processor\ProcessorInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Consent Support entity plugin plugins.
 */
abstract class ConsentSupportPluginBase extends PluginBase implements ConsentSupportPluginInterface, ConfigKeysInterface, ContainerFactoryPluginInterface {

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\consent_support\Processor\ProcessorInterface $processor
   *   The Consent Support processor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ProcessorInterface $processor) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConsentSupportPluginBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('consent_support.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormSubmitHandlerAttachLocations(): array {
    return [['actions', 'submit', '#submit']];
  }

  /**
   * {@inheritdoc}
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void {
    $this->processor->getCookiePro()->addConfigCacheTags($build);
  }

}
