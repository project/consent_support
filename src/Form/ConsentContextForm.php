<?php

namespace Drupal\consent_support\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Add/edit form for Consent Context entity.
 */
class ConsentContextForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->label() ?? '',
      '#description' => $this->t('The human-readable name of this context. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['\Drupal\consent_support\Entity\ConsentContext', 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\consent_support\Entity\ConsentContextInterface $context */
    $context = $this->entity;

    $context->setLabel(trim($context->label()));
    $status = $context->save();

    $params = [
      '%label' => $context->label(),
    ];
    $message = ($status === SAVED_NEW) ? $this->t('Created new context %label.', $params) : $this->t('Updated context %label.', $params);

    $this->messenger()->addMessage($message);
    $form_state->setRedirectUrl($context->toUrl('collection'));

    return $status;
  }

}
