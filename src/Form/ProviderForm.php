<?php

namespace Drupal\consent_support\Form;

use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add/edit form for Provider entity.
 */
class ProviderForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   */
  public function __construct(protected CookieProInterface $cookiePro) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cookiepro_plus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\consent_support\Entity\ProviderInterface $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $entity->label() ?? '',
      '#description' => $this->t('The human-readable name of this provider. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => ['\Drupal\consent_support\Entity\Provider', 'load'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#default_value' => $entity->getCategory(),
      '#options' => $this->cookiePro->getCategoryIdLabels(),
      '#description' => $this->t('The category corresponding with this provider.'),
      '#required' => TRUE,
    ];

    // @todo More examples.
    $description = $this->t('A regular expression to match the domain(s) of the provider. For example %example to match against YouTube domains in general and the localized YouTu.be.', ['%example' => '/youtube|youtu\.be/']);
    $form['expression'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expression'),
      '#default_value' => $entity->getExpression(),
      '#description' => $description,
      '#required' => TRUE,
      '#size' => 30,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\consent_support\Entity\ProviderInterface $provider */
    $provider = $this->entity;

    $expression = trim($provider->getExpression());
    $provider->setExpression($expression);

    $provider->setLabel(trim($provider->label()));
    $status = $provider->save();

    $params = [
      '%label' => $provider->label(),
    ];
    $message = ($status === SAVED_NEW) ? $this->t('Created new provider %label.', $params) : $this->t('Updated provider %label.', $params);

    $this->messenger()->addMessage($message);
    $form_state->setRedirectUrl($provider->toUrl('collection'));

    return $status;
  }

}
