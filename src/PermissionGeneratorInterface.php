<?php

namespace Drupal\consent_support;

/**
 * Interface PermissionGeneratorInterface.
 *
 * Defines permission generator for plugins that add Consent Support.
 */
interface PermissionGeneratorInterface {

  /**
   * Return an array of per-entity Consent Support permissions.
   *
   * @return array
   *   An array of permissions
   */
  public function permissions(): array;

}
