<?php

namespace Drupal\consent_support;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CookieProNoConsentMessage.
 *
 * Service to get no-consent messages for iframes and scripts.
 */
class NoConsentMessage implements NoConsentMessageInterface {

  /**
   * The value from the no-consent url query parameter 'context'.
   *
   * @var string|null
   */
  protected ?string $context;

  /**
   * Current langcode.
   *
   * @var string
   */
  protected string $langcode;

  /**
   * Constructs a CookieProNoConsentMessage object.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   */
  public function __construct(
    protected CookieProInterface $cookiePro,
    protected LanguageManagerInterface $language_manager,
    protected RendererInterface $renderer,
    protected RequestStack $request_stack,
  ) {

    $this->langcode = $language_manager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    $context = (string) $request_stack->getCurrentRequest()->query->get('context');
    $this->context = empty($context) ? NULL : Xss::filter($context);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    // @todo When we make the categories manageable, those (config) cache tags
    // need to be added here.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {

    return [
      'languages:language_content',
      'url.query_args:context',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getContextCssClass(string $context = NULL): string {

    if (empty($context)) {
      $context = $this->context;
    }

    return Html::cleanCssIdentifier("consent-support-no-consent-message--$context");
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageForIframe(string $category_key, string $context = NULL, string $langcode = NULL): array {
    return $this->getMessageBase($category_key, $context, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageForScript(string $category_key, string $context = NULL, string $langcode = NULL): array {
    return $this->getMessageBase($category_key, $context, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageWrapperClassForScript(string $category_key): string {
    return "hide-for-optanon-category-$category_key";
  }

  /**
   * {@inheritdoc}
   */
  public function getNoConsentPageUrl(string $category_key, string $context = NULL, bool $absolute = TRUE, string $langcode = NULL): Url {

    $route_parameters = [
      'category_key' => $category_key,
    ];

    $options = [];
    if (!empty($context)) {
      $options['query'] = [
        'context' => $context,
      ];
    }

    if ($absolute) {
      $options['absolute'] = TRUE;
    }

    if (!empty($langcode)) {
      $options['langcode'] = $langcode;
    }

    return Url::fromRoute('consent_support.no_consent', $route_parameters, $options);
  }

  /**
   * Get render array for no-consent message.
   *
   * @param string $category_key
   *   The category ID key.
   * @param string|null $context
   *   A context value to pass to the no-consent message template.
   * @param string|null $langcode
   *   The desired translation langcode. Defaults to current interface language.
   *
   * @return array
   *   The render array.
   */
  protected function getMessageBase(string $category_key, string $context = NULL, string $langcode = NULL): array {

    $langcode = $langcode ?? $this->langcode;

    $category_labels = $this->cookiePro->getCategoryIdLabels($langcode);
    $label = $category_labels[$category_key] ?? NULL;
    $category_names = $this->cookiePro->getCategoryIdNames($langcode);
    $name = $category_names[$category_key] ?? NULL;

    // Pre-render settings button and link, so they can be used in translatable
    // strings until https://www.drupal.org/project/drupal/issues/2334319 is
    // solved.
    $consent_settings_button = [
      '#theme' => 'cookiepro_consent_settings_button',
      '#langcode' => $this->langcode,
    ];

    $consent_settings_link = [
      '#theme' => 'cookiepro_consent_settings_link',
      '#langcode' => $this->langcode,
    ];

    // Override the context.
    $original_context = NULL;
    if (!is_null($context)) {
      $original_context = $this->context;
      $this->context = $context;
    }

    $message = [
      '#theme' => 'consent_support_no_consent_message',
      '#category_label' => $label,
      '#category_name' => $name,
      '#context' => $this->context,
      '#context_css_class' => $this->getContextCssClass(),
      '#consent_settings_button' => $this->renderer->renderPlain($consent_settings_button),
      '#consent_settings_link' => $this->renderer->renderPlain($consent_settings_link),
      '#langcode' => $this->langcode,
    ];

    // Prerender the build and prepare as render array with the appropriate
    // cache context that matches the context override, if any.
    $build = [
      '#markup' => $this->renderer->renderPlain($message),
      '#cache' => [
        'tags' => $this->getCacheTags(),
        'contexts' => $this->getCacheContexts(),
        'max-age' => $this->getCacheMaxAge(),
      ],
    ];

    // Restore the context.
    if ($original_context) {
      $this->context = $original_context;
    }

    return $build;
  }

}
