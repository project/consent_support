<?php

namespace Drupal\consent_support\Preprocessor;

use Drupal\consent_support\Config\ConfigManager;
use Drupal\consent_support\Plugin\ConsentSupportPluginManager;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Preprocessor.
 *
 * Default implementation for PreprocessorInterface.
 */
class Preprocessor implements PreprocessorInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\consent_support\Plugin\ConsentSupportPluginManager $entityPluginManager
   *   The plugin manager.
   * @param \Drupal\consent_support\Config\ConfigManager $configManager
   *   The config manager.
   */
  public function __construct(protected ConsentSupportPluginManager $entityPluginManager, protected ConfigManager $configManager) {}

  /**
   * {@inheritdoc}
   */
  public function prepareExtendedSupport(array &$build, ConfigEntityInterface|FieldableEntityInterface $entity, EntityViewDisplayInterface $display): void {

    if (in_array($entity->getEntityTypeId(), $this->entityPluginManager->loadSupportedEntityTypeIds())) {

      /** @var \Drupal\consent_support\Entity\ConfigInterface $config */
      $config = $this->configManager->loadConsentSupportConfig($entity);
      if ($config->getEnabled() || $config->getForceCategory()) {

        /** @var \Drupal\consent_support\Plugin\ConsentSupportPluginInterface $plugin */
        $plugin = $this->entityPluginManager->createInstance("consent_support_{$entity->getEntityTypeId()}");
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $plugin->processDisplay($build, $entity, $display, $config);
      }
    }
  }

}
