<?php

namespace Drupal\consent_support\Preprocessor;

use Drupal\consent_support\Config\ConfigKeysInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Interface ESPreprocessorInterface.
 *
 * Defines preprocessor to check entities if processing for CookiePro Extended
 * Support is required and, if so, initiate processing.
 */
interface PreprocessorInterface extends ConfigKeysInterface {

  /**
   * Prepare entity display for Consent Support.
   *
   * @param array $build
   *   The render array.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface|\Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display.
   *
   * @see hook_entity_view_alter()
   */
  public function prepareExtendedSupport(array &$build, ConfigEntityInterface|FieldableEntityInterface $entity, EntityViewDisplayInterface $display): void;

}
