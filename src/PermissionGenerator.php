<?php

namespace Drupal\consent_support;

use Drupal\consent_support\Plugin\ConsentSupportPluginManager;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ESPermissionGenerator.
 *
 * Implements generic permission generator.
 */
class PermissionGenerator implements PermissionGeneratorInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * ESPermissionGenerator Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\consent_support\Plugin\ConsentSupportPluginManager $entityPluginManager
   *   The Consent Support entity plugin manager.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager, protected ConsentSupportPluginManager $entityPluginManager) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): PermissionGeneratorInterface {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.consent_support_entity_plugin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function permissions(): array {
    $permissions = [];

    foreach ($this->entityPluginManager->getDefinitions() as $definition) {
      try {
        $entity_type = $this->entityTypeManager->getStorage($definition['entityType'])->getEntityType();

        $title = $this->t('Override processing configuration on @entity_type', ['@entity_type' => $entity_type->getPluralLabel()]);
        $description = $this->t('This allows users to assign incorrect categories for the content, which can lead to incorrect consent behaviour.');

        if ($entity_type instanceof ConfigEntityTypeInterface) {
          $title = $this->t('Set default processing configuration for @entity_type', ['@entity_type' => $entity_type->getPluralLabel()]);
          $description = NULL;
        }

        $permissions += [
          'consent support override ' . $definition['entityType'] => [
            'title' => $title,
            'description' => $description,
            'restrict access' => TRUE,
          ],
        ];
      }
      catch (\Exception $e) {
        // Do nothing.
      }
    }

    return $permissions;
  }

}
