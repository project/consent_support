<?php

namespace Drupal\consent_support;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List Builder for Consent Context entity.
 */
class ConsentContextListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {

    $header = [
      'id' => $this->t('ID'),
      'label' => $this->t('Label'),
    ];

    return array_merge($header, parent::buildHeader());
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {

    $row = [
      'id' => $entity->id(),
      'label' => $entity->label(),
    ];

    return array_merge($row, parent::buildRow($entity));
  }

}
