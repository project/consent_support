<?php

namespace Drupal\consent_support\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Consent Support Config entity.
 *
 * @ConfigEntityType(
 *   id = "consent_support_config",
 *   label = @Translation("Consent Support configuration"),
 *   handlers = {},
 *   config_prefix = "config",
 *   admin_permission = "administer consent support configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {},
 *    config_export = {
 *     "id",
 *     "uuid",
 *     "allow_override",
 *     "category_key",
 *     "context",
 *     "enabled",
 *     "force_category",
 *   }
 * )
 */
class Config extends ConfigEntityBase implements ConfigInterface {

  /**
   * The entity ID, eg. 'article'.
   *
   * @var string
   */
  protected string $entity_id = '';

  /**
   * The entity type ID, eg. 'node_type'.
   *
   * @var string
   */
  protected string $entity_type_id = '';

  /**
   * Create ESConfig entity.
   *
   * @param array $values
   *   Array of values for the entity. Defaults to empty category ID and
   *   context, enabled to TRUE and force category to FALSE.
   *
   * @return \Drupal\consent_support\Entity\ConfigInterface
   *   The config entity.
   */
  public static function create(array $values = []): ConfigInterface {

    $defaults = [
      static::CONF_CATEGORY_KEY => NULL,
      static::CONF_CONTEXT => '',
      static::CONF_ENABLED => 1,
      static::CONF_FORCE_CATEGORY => 0,
    ];
    $values = array_merge($defaults, $values);

    return parent::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    if ($this->entity_type_id && $this->entity_id) {
      $entityTypeManager = $this->entityTypeManager();

      // Create dependency on the bundle.
      $bundle = $entityTypeManager->getDefinition($this->entity_type_id);
      $entity_type = $entityTypeManager->getDefinition($bundle->getBundleOf());

      $bundle_config_dependency = $entity_type->getBundleConfigDependency($this->entity_id);
      $this->addDependency($bundle_config_dependency['type'], $bundle_config_dependency['name']);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryKey(): ?string {
    return $this->{static::CONF_CATEGORY_KEY};
  }

  /**
   * {@inheritdoc}
   */
  public function setCategoryKey(string $category_key): ConfigInterface {
    $this->{static::CONF_CATEGORY_KEY} = $category_key;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): ?string {
    return $this->{static::CONF_CONTEXT};
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(string $context): ConfigInterface {
    $this->{static::CONF_CONTEXT} = $context;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabled(): bool {
    return $this->{static::CONF_ENABLED};
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled(bool $enabled): ConfigInterface {
    $this->{static::CONF_ENABLED} = $enabled;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getForceCategory(): bool {
    return $this->{static::CONF_FORCE_CATEGORY};
  }

  /**
   * {@inheritdoc}
   */
  public function setForceCategory(bool $force_category): ConfigInterface {
    $this->{static::CONF_FORCE_CATEGORY} = $force_category;
    return $this;
  }

}
