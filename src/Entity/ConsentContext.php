<?php

namespace Drupal\consent_support\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Consent Context entity.
 *
 * @ConfigEntityType(
 *   id = "consent_context",
 *   label = @Translation("Consent Context"),
 *   label_collection = @Translation("Consent Contexts"),
 *   label_singular = @Translation("consent context"),
 *   label_plural = @Translation("consent contexts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count consent context",
 *     plural = "@count consent contexts",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\consent_support\ConsentContextAccessControlHandler",
 *     "form" = {
 *        "default" = "Drupal\consent_support\Form\ConsentContextForm",
 *        "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "list_builder" = "Drupal\consent_support\ConsentContextListBuilder",
 *     "route_provider" = {
 *        "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer consent support configuration",
 *   config_prefix = "consent_context",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/system/consent-support/consent-context/add",
 *     "collection" = "/admin/config/system/consent-support/consent-context",
 *     "delete-form" = "/admin/config/system/consent-support/consent-context/{consent_context}/delete",
 *     "edit-form" = "/admin/config/system/consent-support/consent-context/{consent_context}"
 *   }
 * )
 */
class ConsentContext extends ConfigEntityBase implements ConsentContextInterface {

  /**
   * The Consent Context ID (machine name).
   *
   * @var string
   */
  protected string $id;

  /**
   * The Consent Context label.
   *
   * @var string
   */
  protected string $label;

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label): ConsentContextInterface {
    $this->label = $label;
    return $this;
  }

}
