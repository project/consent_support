<?php

namespace Drupal\consent_support\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Provider entity.
 *
 * @ConfigEntityType(
 *   id = "provider",
 *   label = @Translation("Provider"),
 *   label_collection = @Translation("Providers"),
 *   label_singular = @Translation("provider"),
 *   label_plural = @Translation("providers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count provider",
 *     plural = "@count providers",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *        "default" = "Drupal\consent_support\Form\ProviderForm",
 *        "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "list_builder" = "Drupal\consent_support\ProviderListBuilder",
 *     "route_provider" = {
 *        "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer consent support configuration+administer consent support providers",
 *   config_prefix = "provider",
 *   config_export = {
 *     "id",
 *     "label",
 *     "category",
 *     "expression"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/system/consent-support/provider/add",
 *     "collection" = "/admin/config/system/consent-support/provider",
 *     "delete-form" = "/admin/config/system/consent-support/provider/{provider}/delete",
 *     "edit-form" = "/admin/config/system/consent-support/provider/{provider}"
 *   }
 * )
 */
class Provider extends ConfigEntityBase implements ProviderInterface {

  /**
   * The Provider category.
   *
   * @var string|null
   */
  protected ?string $category;

  /**
   * The Provider expression.
   *
   * @var string|null
   */
  protected ?string $expression;

  /**
   * The Provider ID (machine name).
   *
   * @var string
   */
  protected string $id;

  /**
   * The Provider label.
   *
   * @var string
   */
  protected string $label;

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label): ProviderInterface {
    $this->label = $label;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategory(): string {
    return $this->category ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getExpression(): string {
    return $this->expression ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setExpression(string $expression): ProviderInterface {
    $this->expression = $expression;
    return $this;
  }

}
