<?php

namespace Drupal\consent_support\Entity;

use Drupal\consent_support\Config\ConfigKeysInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ConfigInterface.
 *
 * Wraps core Config Entities to make handling of Consent Support config
 * easier.
 */
interface ConfigInterface extends ConfigKeysInterface, ConfigEntityInterface {

  /**
   * Get the category ID.
   *
   * @return string|null
   *   The category ID.
   */
  public function getCategoryKey(): ?string;

  /**
   * Set the category ID.
   *
   * @param string $category_key
   *   The category ID.
   *
   * @return \Drupal\consent_support\Entity\ConfigInterface
   *   The config entity.
   */
  public function setCategoryKey(string $category_key): ConfigInterface;

  /**
   * Get the no-consent message context to add.
   *
   * @return string|null
   *   The value.
   */
  public function getContext(): ?string;

  /**
   * Set the no-consent message context to enforce.
   *
   * @param string $context
   *   The context.
   *
   * @return \Drupal\consent_support\Entity\ConfigInterface
   *   The config entity.
   */
  public function setContext(string $context): ConfigInterface;

  /**
   * Whether we must process for CookiePro support.
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function getEnabled(): bool;

  /**
   * Set whether we must process for CookiePro support.
   *
   * @param bool $enabled
   *   Whether to process for CookiePro.
   *
   * @return \Drupal\consent_support\Entity\ConfigInterface
   *   The config entity.
   */
  public function setEnabled(bool $enabled): ConfigInterface;

  /**
   * Whether we must force processing for CookiePro support.
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function getForceCategory(): bool;

  /**
   * Set whether we must force processing for CookiePro support.
   *
   * @param bool $force_category
   *   Whether to force processing for CookiePro.
   *
   * @return \Drupal\consent_support\Entity\ConfigInterface
   *   The config entity.
   */
  public function setForceCategory(bool $force_category): ConfigInterface;

}
