<?php

namespace Drupal\consent_support\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Consent Context entity.
 */
interface ConsentContextInterface extends ConfigEntityInterface {

  /**
   * The script Consent Context ID.
   *
   * @var string
   */
  const CONTEXT_SCRIPT = 'script';

  /**
   * The video Consent Context ID.
   *
   * @var string
   */
  const CONTEXT_VIDEO = 'video';

  /**
   * Set the Consent Context label.
   *
   * @param string $label
   *   The label.
   *
   * @return \Drupal\consent_support\Entity\ConsentContextInterface
   *   The Consent Context.
   */
  public function setLabel(string $label): ConsentContextInterface;

}
