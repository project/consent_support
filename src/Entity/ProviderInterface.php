<?php

namespace Drupal\consent_support\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Provider entity.
 */
interface ProviderInterface extends ConfigEntityInterface {

  /**
   * Get the Provider category.
   *
   * @return string
   *   The category.
   */
  public function getCategory(): string;

  /**
   * Get the Provider expression.
   *
   * @return string
   *   The expression.
   */
  public function getExpression(): string;

  /**
   * Set the Provider expression.
   *
   * @param string $expression
   *   The expression.
   *
   * @return \Drupal\consent_support\Entity\ProviderInterface
   *   The Provider.
   */
  public function setExpression(string $expression): ProviderInterface;

  /**
   * Set the Provider label.
   *
   * @param string $label
   *   The label.
   *
   * @return \Drupal\consent_support\Entity\ProviderInterface
   *   The Provider.
   */
  public function setLabel(string $label): ProviderInterface;

}
