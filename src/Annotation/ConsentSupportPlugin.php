<?php

namespace Drupal\consent_support\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Consent Support entity plugin item annotation object.
 *
 * @see \Drupal\consent_support\Plugin\ConsentSupportPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class ConsentSupportPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The string ID of the affected entity.
   *
   * @var string
   */
  public string $entityType;

}
