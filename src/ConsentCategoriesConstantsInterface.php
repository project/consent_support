<?php

namespace Drupal\consent_support;

use Drupal\cookiepro_plus\CookieProConstantsInterface;

/**
 * Interface CookieProConstantsInterface.
 *
 * Contains all module constants.
 *
 * For now, we refer to Drupal\cookiepro_plus\CookieProConstantsInterface to
 * keep things in sync.
 */
interface ConsentCategoriesConstantsInterface {

  /**
   * The 'category functional' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_FUNCTIONAL = CookieProConstantsInterface::CONF_CATEGORY_FUNCTIONAL;

  /**
   * The 'category performance' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_PERFORMANCE = CookieProConstantsInterface::CONF_CATEGORY_PERFORMANCE;

  /**
   * The 'category social media' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_SOCIAL_MEDIA = CookieProConstantsInterface::CONF_CATEGORY_SOCIAL_MEDIA;

  /**
   * The 'category strictly necessary' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_STRICTLY_NECESSARY = CookieProConstantsInterface::CONF_CATEGORY_STRICTLY_NECESSARY;

  /**
   * The 'category targeting' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_TARGETING = CookieProConstantsInterface::CONF_CATEGORY_TARGETING;

}
