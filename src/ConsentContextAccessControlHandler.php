<?php

namespace Drupal\consent_support;

use Drupal\consent_support\Entity\ConsentContextInterface;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access handler for Consent Context entity.
 */
class ConsentContextAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    $access = parent::checkAccess($entity, $operation, $account);

    if ($operation === 'delete') {

      // Consent Contexts shipping with the module and used by submodule(s), so
      // they may not be deleted.
      $disallowed_contexts = [
        ConsentContextInterface::CONTEXT_SCRIPT,
        ConsentContextInterface::CONTEXT_VIDEO,
      ];
      if (in_array($entity->id(), $disallowed_contexts)) {
        $access = AccessResultForbidden::forbidden('This Consent Context cannot be deleted.');
      }
    }

    return $access;
  }

}
