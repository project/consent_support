<?php

namespace Drupal\consent_support\Processor;

use Drupal\Component\Utility\Html;
use Drupal\consent_support\Config\ConfigKeysInterface;
use Drupal\consent_support\ConsentCategoriesConstantsInterface;
use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\consent_support\NoConsentMessageInterface;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Template\Attribute;
use Psr\Log\LoggerInterface;

/**
 * Class Preprocessor.
 *
 * Default implementation of ProcessorInterface.
 */
class Processor implements ProcessorInterface {

  /**
   * The Provider mapping.
   *
   * @var string[]
   */
  protected array $providerMapping;

  /**
   * Preprocessor constructor.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   * @param \Drupal\consent_support\NoConsentMessageInterface $noConsentMessage
   *   The No-Consent Message service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    protected CookieProInterface $cookiePro,
    protected NoConsentMessageInterface $noConsentMessage,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerInterface $logger,
    protected RendererInterface $renderer,
  ) {
    $this->providerMapping = $this->prepareProviderMapping($entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheTags(array &$build): void {

    if (!isset($build['#cache']['tags'])) {
      $build['#cache']['tags'] = [];
    }

    $build['#cache']['tags'] = Cache::mergeTags($build['#cache']['tags'], $this->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function attachNoConsentLibrary(array &$build): void {

    if (!isset($build['#attached']['library'])
      || !in_array('consent_support/consent_support.cookiepro_no_consent', $build['#attached']['library'])
    ) {
      $build['#attached']['library'][] = 'consent_support/consent_support.cookiepro_no_consent';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    // If a provider is added or changed, it might affect the process results.
    return ['provider_list'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryKeyByProvider(string $provider): ?string {
    // Media providers may contain uppercase.
    $provider = strtolower($provider);

    $mapping = $this->providerMapping;
    if (isset($this->providerMapping[$provider])) {
      return $mapping[$provider]['category'];
    }

    $this->logger->debug('Missing provider mapping for provider @provider.', ['@provider' => $provider]);

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryKeyByProviderUrl(string $url): ?string {

    $matches = [];
    foreach ($this->providerMapping as $map) {

      preg_match($map['expression'], $url, $matches);

      if ($matches) {
        return $map['category'];
      }
    }

    $this->logger->debug('Missing provider mapping for url @url', ['@url' => $url]);

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getClassName(string $category_key): string {
    return $this->cookiePro->getCategoryCssClass($category_key);
  }

  /**
   * {@inheritdoc}
   */
  public function getCookiePro(): CookieProInterface {
    return $this->cookiePro;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNamesToIgnore(): array {
    return [
      // Ignore common string base fields:
      // Ours.
      ConfigKeysInterface::CONF_CATEGORY_KEY,
      ConfigKeysInterface::CONF_CONTEXT,
      // Paragraphs.
      'parent_id',
      'parent_type',
      'parent_field_name',
      'behavior_settings',
      // Rabbit Hole.
      'rh_action',
      'rh_redirect',
      // Revisions.
      'revision_log_message',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getNoConsentPageUrl(string $category_key, string $context = NULL): string {
    return $this->noConsentMessage->getNoConsentPageUrl($category_key, $context)->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void {

    $this->getCookiePro()->addConfigCacheTags($build);

    $fields = $entity->getFieldDefinitions();
    // Skip our own additional fields.
    $fields = array_diff_key($fields, array_flip($this->getFieldNamesToIgnore()));

    $supported_types = [
      'iframe',
      'string',
      'string_long',
      'text',
      'text_long',
      'text_with_summary',
    ];

    foreach ($fields as $field_name => $field) {

      // Find the fields containing Html elements. Any string or text field in
      // the build array is a potential target.
      if (in_array($field->getType(), $supported_types) && isset($build[$field_name])) {

        /** @var \Drupal\Core\Field\FieldItemInterface $item */
        $field_items = $entity->get($field_name);
        foreach ($field_items as $index => $item) {

          try {

            // Support Iframe 8.x-2.x.
            if ($field->getType() === 'iframe') {
              $this->processIframeRenderArray($build[$field_name][$index], $config);
            }
            // Support text fields.
            else {
              switch ($build[$field_name][$index]['#type']) {

                case 'inline_template':

                  // Attach provider cache tags if a category was forced.
                  if ($this->processHtmlAsString($build[$field_name][$index]['#context']['value'], $config)) {
                    $this->addCacheTags($build[$field_name]);
                  }

                  $this->getCookiePro()->addConfigCacheTags($build[$field_name]);
                  $this->attachNoConsentLibrary($build);

                  break;

                case 'processed_text':

                  // Attach provider cache tags if a category was forced.
                  if ($this->processHtmlAsString($build[$field_name][$index]['#text'], $config)) {
                    $this->addCacheTags($build[$field_name]);
                  }

                  $this->getCookiePro()->addConfigCacheTags($build[$field_name]);
                  $this->attachNoConsentLibrary($build);

                  break;

                default:
              }
            }
          }
          catch (\Exception $exception) {
            // Silent fails for now.
            continue;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processHtmlAsString(string &$string, ConfigInterface $config): bool {

    $has_forced_category = FALSE;

    // Known Html elements we need to alter.
    $tag_names = [
      'iframe',
      'script',
    ];

    // Default category and class name we'll need to add.
    $category_key = $config->getCategoryKey();
    $class_name = $category_key ? $this->getClassName($category_key) : '';
    $context = $config->getContext();
    $html = Html::load($string);

    foreach ($tag_names as $name) {

      $elements = $html->getElementsByTagName($name);
      /** @var \DOMElement $element */
      foreach ($elements as $element) {

        // Determine final category and class name.
        $category_key_final = $category_key;
        $class_name_final = $class_name;

        $src = $element->getAttribute('src');

        // Force category if required.
        if ($config->getForceCategory()) {

          $category_key_forced = $this->getCategoryKeyByProviderUrl($src);
          if ($category_key_forced) {

            $category_key_final = $category_key_forced;
            $class_name_final = $this->getClassName($category_key_forced);

            $has_forced_category = TRUE;
          }
          // If not forced, processing might still be disabled.
          elseif (!$config->getEnabled()) {
            continue;
          }
        }

        // Nothing to block, continue to the next element.
        if (empty($category_key_final) ||
          $category_key_final === ConsentCategoriesConstantsInterface::CONF_CATEGORY_STRICTLY_NECESSARY
        ) {
          continue;
        }

        // Final category ID.
        $category_id_final = $this->cookiePro->getCategoryIds()[$category_key_final];

        // Rewrite css class with category class name.
        $class = $element->getAttribute('class');
        $class = $class ? "$class_name_final $class" : $class_name_final;

        switch ($name) {

          case 'iframe':
            /*
             *  Iframe rewrite:
             *
             * - Append class with category ID.
             * - Move src value to data-src
             * - Set src to our custom no-consent page.
             * @see https://community.cookiepro.com/s/article/UUID-3dc06342-ff6b-3578-5024-6d7c05d03283?topicId=0TO1Q000000ssbQWAQ
             */
            $element->setAttribute('class', $class);
            $element->setAttribute('data-src', $src);
            $element->setAttribute('src', $this->getNoConsentPageUrl($category_key_final, $context));
            break;

          case 'script':

            /*
             * 'External' script rewrite:
             *
             * - Remove src, async attributes
             * - Load external script using the OneTrust.InsertScript, but let
             *   it be executed as an inline script. This circumvents the need
             *   to add all these dynamically added scripts to OptanonWrapper,
             *   but will cause the same script to be loaded multiple times
             *   if included multiple on a single page.
             * - Append class with category ID.
             * - Change type to plain text.
             * @see https://community.cookiepro.com/s/article/UUID-730ad441-6c4d-7877-7f85-36f1e801e8ca?topicId=0TO1Q000000ssKJWAY#idm2046
             */
            if ($src) {

              $element->removeAttribute('src');
              $element->removeAttribute('async');
              $element->nodeValue = "OneTrust.InsertScript('$src', 'body', null, null, '$category_id_final');";

              $element->setAttribute('class', $class);
              $element->setAttribute('type', 'text/plain');
            }

            /*
             * Inline scripts rewrite:
             * - Append class with category ID.
             * - Change type to plain text.
             * @see https://community.cookiepro.com/s/article/UUID-730ad441-6c4d-7877-7f85-36f1e801e8ca?topicId=0TO1Q000000ssKJWAY#idm2013
             */
            else {
              $element->setAttribute('class', $class);
              $element->setAttribute('type', 'text/plain');
            }

            // To inject no-consent message for script, first add a wrapper for
            // CookiePro the remove when consent is given.
            $no_consent_wrapper = $html->createElement('div', '');
            $classes = implode(' ', [
              $this->noConsentMessage->getMessageWrapperClassForScript($category_id_final),
            ]);
            $no_consent_wrapper->setAttribute('class', $classes);

            // Prepare the no-consent message.
            $message = $this->noConsentMessage->getMessageForScript($category_key_final, $context);
            $message = $this->renderer->renderPlain($message);

            // To retain template suggestions, if present, create a DOMDocument
            // without doctype, html and body tag, so we can append all the
            // nodes of this document into the wrapper.
            // Also, suppress invalid tag errors on creation.
            $document = new \DOMDocument();
            @$document->loadHTML($message, LIBXML_HTML_NOIMPLIED);
            $nodes = $document->childNodes;
            $length = $nodes->length;
            for ($i = 0; $i < $length; $i++) {

              $message_element = $nodes->item($i);
              $message_element = $html->importNode($message_element, TRUE);
              if ($message_element !== FALSE) {
                $no_consent_wrapper->appendChild($message_element);
              }
            }

            $element->parentNode->insertBefore($no_consent_wrapper, $element);
            break;

          default:
            // Nothing for now.
        }
      }
    }

    $string = Html::serialize($html);
    return $has_forced_category;
  }

  /**
   * {@inheritdoc}
   */
  public function processIframeRenderArray(array &$render_array, ConfigInterface $config): void {

    if ($config->getEnabled() || $config->getForceCategory()) {
      $this->getCookiePro()->addConfigCacheTags($render_array);

      $src = $render_array['#src'] ?? ($render_array['#atrributes']['src'] ?? NULL);
      if ($src) {

        $category_key = $config->getCategoryKey();

        // Force category if required.
        if ($config->getForceCategory()) {

          // Category depends on Providers.
          $this->addCacheTags($render_array);

          $category_key_forced = $this->getCategoryKeyByProviderUrl($src);
          if ($category_key_forced) {
            $category_key = $category_key_forced;
          }
          // If not forced, processing might still be disabled.
          elseif (!$config->getEnabled()) {
            return;
          }
        }

        // Nothing to block, continue to the next element.
        if (empty($category_key) ||
          $category_key === ConsentCategoriesConstantsInterface::CONF_CATEGORY_STRICTLY_NECESSARY
        ) {
          return;
        }

        $class = $this->getClassName($category_key);
        $no_consent_url = $this->getNoConsentPageUrl($category_key, $config->getContext());

        /*
         *  Iframe rewrite:
         *
         * - Append class with category ID.
         * - Move src value to data-src
         * - Set src to our custom no-consent page.
         * @see https://community.cookiepro.com/s/article/UUID-3dc06342-ff6b-3578-5024-6d7c05d03283?topicId=0TO1Q000000ssbQWAQ
         */
        if (isset($render_array['#src'])) {
          $render_array['#src'] = $no_consent_url;
        }

        $attributes = &$render_array['#attributes'];
        if ($attributes instanceof Attribute) {
          $attributes->addClass('class', $class);
          $attributes->setAttribute('data-src', $src);

          if ($attributes->hasAttribute('src')) {
            $attributes->setAttribute('src', $no_consent_url);
          }
        }
        elseif (is_array($render_array['#attributes'])) {
          $render_array['#attributes']['data-src'] = $src;
          $render_array['#attributes']['class'][] = $class;

          if (isset($render_array['#atrributes']['src'])) {
            $render_array['#atrributes']['src'] = $no_consent_url;
          }
        }
      }
    }
  }

  /**
   * Build the provider mapping.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return array
   *   Array of provider category and expression keyed by their provider id.
   */
  public static function prepareProviderMapping(EntityTypeManagerInterface $entity_type_manager): array {

    $mapping = [];

    try {
      /** @var \Drupal\consent_support\Entity\ProviderInterface[] $providers */
      $providers = $entity_type_manager->getStorage('provider')->loadMultiple();

      foreach ($providers as $provider) {
        $mapping[$provider->id()] = [
          'category' => $provider->getCategory(),
          'expression' => $provider->getExpression(),
        ];
      }
    }
    catch (\Exception $exception) {
      // Silent fail, for now.
    }

    return $mapping;
  }

}
