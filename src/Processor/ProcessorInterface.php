<?php

namespace Drupal\consent_support\Processor;

use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Psr\Log\LoggerInterface;

/**
 * Defines processor to process entity content for Consent Support.
 */
interface ProcessorInterface {

  /**
   * Add Provider cache tags to given render array.
   *
   * @param array $build
   *   The build render array.
   */
  public function addCacheTags(array &$build): void;

  /**
   * Add the No Consent library to given render array.
   *
   * This meant for scripts, that are manipulated inline.
   *
   * @param array $build
   *   The render array.
   */
  public function attachNoConsentLibrary(array &$build): void;

  /**
   * The cache tags associated with Providers.
   *
   * @return string[]
   *   A set of cache tags.
   */
  public function getCacheTags(): array;

  /**
   * Get category key for given provider.
   *
   * @param string $provider
   *   The provider.
   *
   * @return string|null
   *   The category key.
   */
  public function getCategoryKeyByProvider(string $provider): ?string;

  /**
   * Get category key for given provider url.
   *
   * @param string $url
   *   The provider url.
   *
   * @return string|null
   *   The category key.
   */
  public function getCategoryKeyByProviderUrl(string $url): ?string;

  /**
   * Get consent category class using given consent category ID key.
   *
   * @param string $category_key
   *   The category ID key.
   *
   * @return string
   *   The class to add based on the consent category ID.
   */
  public function getClassName(string $category_key): string;

  /**
   * Get the CookiePro service.
   *
   * @return \Drupal\cookiepro_plus\CookieProInterface
   *   The CookiePro service.
   */
  public function getCookiePro(): CookieProInterface;

  /**
   * Lists field names of known fields we can ignore during processing.
   *
   * @return array
   *   The field names.
   */
  public function getFieldNamesToIgnore(): array;

  /**
   * Get the consent_support logger channel.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger(): LoggerInterface;

  /**
   * Get url to bare page with no-consent message.
   *
   * @param string $category_key
   *   The category ID key.
   * @param string|null $context
   *   A context parameter to add to the url. This will be passed on to the
   *   no-consent page and message template.
   *
   * @return string
   *   Absolute url.
   */
  public function getNoConsentPageUrl(string $category_key, string $context = NULL): string;

  /**
   * Alters render array for given entity to support CookiePro.
   *
   * @param array $build
   *   The render array.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display.
   * @param \Drupal\consent_support\Entity\ConfigInterface $config
   *   The Consent Support Config object for the entity.
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void;

  /**
   * Alter html elements in string for Consent Support.
   *
   * @param string $string
   *   (Field) Content to alter for Consent Support.
   * @param \Drupal\consent_support\Entity\ConfigInterface $config
   *   The entity config.
   *
   * @return bool
   *   TRUE if category was forced.
   */
  public function processHtmlAsString(string &$string, ConfigInterface $config): bool;

  /**
   * Alter iframe render array for Consent Support.
   *
   * Iframe source needs to be present either as $render_array['#src'] or as
   * 'src' attribute in $render_array['#atrributes'].
   *
   * @param array $render_array
   *   The render array of the iframe.
   * @param \Drupal\consent_support\Entity\ConfigInterface $config
   *   The entity config.
   */
  public function processIframeRenderArray(array &$render_array, ConfigInterface $config): void;

}
