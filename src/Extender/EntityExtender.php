<?php

namespace Drupal\consent_support\Extender;

use Drupal\consent_support\Plugin\ConsentSupportPluginManager;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class EntityExtender.
 *
 * Default implementation of the Consent Support entity extender.
 */
class EntityExtender implements EntityExtenderInterface {

  use StringTranslationTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\consent_support\Plugin\ConsentSupportPluginManager $entityPluginManager
   *   Consent Support plugin manager.
   */
  public function __construct(protected ConsentSupportPluginManager $entityPluginManager) {}

  /**
   * {@inheritdoc}
   */
  public function getExtendedSupportFields(string $entity_type_id): array {

    // Load supported entity types uncached, or we will get problems during
    // install and config imports.
    $entity_types = $this->entityPluginManager->loadSupportedEntityTypeIds();

    if (in_array($entity_type_id, $entity_types)) {
      return static::getGeneralExtraFields();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function getGeneralExtraFields(): array {

    $fields[static::CONF_CATEGORY_KEY] = BaseFieldDefinition::create('string')
      ->setName(static::CONF_CATEGORY_KEY)
      ->setLabel(new TranslatableMarkup('Consent category ID key'))
      ->setDescription(new TranslatableMarkup('Specifies which consent category must be applied.'));

    $fields[static::CONF_CONTEXT] = BaseFieldDefinition::create('string')
      ->setName(static::CONF_CONTEXT)
      ->setLabel(new TranslatableMarkup('Consent context'))
      ->setDescription(new TranslatableMarkup('Specifies which consent context must be applied.'));

    $fields[static::CONF_ENABLED] = BaseFieldDefinition::create('integer')
      ->setName(static::CONF_ENABLED)
      ->setLabel(new TranslatableMarkup('Enable processing'))
      ->setDescription(new TranslatableMarkup('Specifies if processing for CookiePro must be performed.'));

    return $fields;
  }

}
