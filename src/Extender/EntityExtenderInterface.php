<?php

namespace Drupal\consent_support\Extender;

use Drupal\consent_support\Config\ConfigKeysInterface;

/**
 * Interface ESEntityExtenderInterface.
 *
 * Defines entity extensions for Consent Support.
 */
interface EntityExtenderInterface extends ConfigKeysInterface {

  /**
   * Return fields added by Consent Support.
   *
   * @param string $entity_type_id
   *   The string ID of the entity type.
   *
   * @return array
   *   An array of general extra fields. Empty if the entity type is not
   *   supported.
   */
  public function getExtendedSupportFields(string $entity_type_id): array;

  /**
   * Get extra fields that should be applied to all Consent Support entities.
   */
  public static function getGeneralExtraFields(): array;

}
