<?php

namespace Drupal\consent_support\Extender;

use Drupal\consent_support\Config\ConfigManager;
use Drupal\consent_support\ConsentCategoriesConstantsInterface;
use Drupal\consent_support\Plugin\ConsentSupportPluginManager;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Class FormExtender.
 *
 * Default implementation of the Consent Support form extender.
 */
class FormExtender implements FormExtenderInterface {

  use StringTranslationTrait;

  /**
   * The bundle info of all entity types.
   *
   * @var array
   *
   * @see \Drupal\Core\Entity\EntityTypeBundleInfoInterface::getAllBundleInfo()
   */
  protected array $bundleInfo;

  /**
   * Constructor.
   *
   * @param \Drupal\consent_support\Config\ConfigManager $configManager
   *   The Consent Support config manager.
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   The entity type bundle info provider.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   * @param \Drupal\consent_support\Plugin\ConsentSupportPluginManager $entityPluginManager
   *   The Consent Support entity plugin manager.
   */
  public function __construct(
    protected ConfigManager $configManager,
    protected CookieProInterface $cookiePro,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfo $entityTypeBundleInfo,
    protected LoggerInterface $logger,
    protected ConsentSupportPluginManager $entityPluginManager,
  ) {
    $this->bundleInfo = $entityTypeBundleInfo->getAllBundleInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function addExtendedSupportOptionsToEntityForm(array &$attach, EntityInterface $entity): void {
    $this->addExtendedSupportOptionsToForm($attach, $entity->getEntityType()->id(), $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function handleFormSubmit(array $form, FormStateInterface $form_state): void {

    // We only need to handle bundle config, as entity overrides are saved
    // on the entity itself and handled by the default submit handler.
    if ($form_state->getValue(static::HIDDEN_FIELD_IS_BUNDLE)) {
      $entity = NULL;

      if (method_exists($form_state->getFormObject(), 'getEntity')) {
        $entity = $form_state->getFormObject()->getEntity();
      }

      $this->configManager->saveConsentSupportConfig(
        [
          static::CONF_ALLOW_OVERRIDE => $form_state->getValue(static::CONF_ALLOW_OVERRIDE),
          static::CONF_CATEGORY_KEY => $form_state->getValue(static::CONF_CATEGORY_KEY),
          static::CONF_CONTEXT => $form_state->getValue(static::CONF_CONTEXT),
          static::CONF_ENABLED => $form_state->getValue(static::CONF_ENABLED),
          static::CONF_FORCE_CATEGORY => $form_state->getValue(static::CONF_FORCE_CATEGORY),
        ],
        $form_state->getValue(static::HIDDEN_FIELD_ENTITY_TYPE),
        ($entity ? $entity->id() : NULL)
      );
    }
  }

  /**
   * Common functionality for adding Consent Support options to forms.
   *
   * @param array $attach
   *   The form that the Consent Support form should be attached to.
   * @param string $entity_type_id
   *   The string ID of the entity type for the form, e.g. 'node'.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that we're adding the form to.
   */
  protected function addExtendedSupportOptionsToForm(array &$attach, string $entity_type_id, EntityInterface $entity): void {

    if (!$entity instanceof ConfigEntityInterface && !$entity instanceof FieldableEntityInterface) {
      return;
    }

    $category_id_options = $this->cookiePro->getCategoryIdLabels();
    unset($category_id_options[ConsentCategoriesConstantsInterface::CONF_CATEGORY_STRICTLY_NECESSARY]);
    $category_id_description = $this->t('The category to be applied to embedded iframe and script elements by default.');
    $category_id_title = $this->t('Consent category');

    $enable_title = $this->t('Enable processing');

    $context_title = $this->t('Consent context');
    $context_description = $this->t("The consent context determines the 'no-consent message' and its display.");

    // Get the available Consent Contexts.
    $consent_context_options = [];
    try {
      $consent_contexts = $this->entityTypeManager->getStorage('consent_context')->loadMultiple();
      foreach ($consent_contexts as $consent_context) {
        $consent_context_options[$consent_context->id()] = $consent_context->label();
      }
      unset($consent_contexts);
    }
    catch (\Exception $exception) {
      // Silent fail, for now.
    }

    $is_bundle = ($entity instanceof ConfigEntityInterface);

    // Wrap everything in a details (will show as tab).
    $title = $this->t('Consent Support');
    $form['consent_support'] = [
      '#type' => 'details',
      '#title' => $title,
      '#open' => TRUE,
      '#group' => $is_bundle ? 'additional_settings' : 'advanced',
      '#attributes' => ['class' => ['consent-support-config-form']],
    ];

    $form['consent_support'][static::HIDDEN_FIELD_IS_BUNDLE] = [
      '#type' => 'hidden',
      '#value' => $is_bundle,
    ];

    $form['consent_support'][static::HIDDEN_FIELD_ENTITY_TYPE] = [
      '#type' => 'hidden',
      '#value' => $entity_type_id,
    ];

    // Bundle config.
    if ($is_bundle) {

      $bundle_config = $this->configManager->loadConsentSupportImmutableConfig($entity_type_id, (string) $entity->id());

      $description = $this->t('Force processing and category application when a known provider is detected.<br><strong>Warning</strong>: This option overrules any other configuration on the bundle or its entities.');
      $form['consent_support'][static::CONF_FORCE_CATEGORY] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Force consent category by provider'),
        '#default_value' => $bundle_config->get(static::CONF_FORCE_CATEGORY),
        '#description' => $description,
      ];

      $form['consent_support'][static::CONF_ENABLED] = [
        '#type' => 'checkbox',
        '#title' => $enable_title,
        '#default_value' => $bundle_config->get(static::CONF_ENABLED),
        '#description' => $this->t('Process embedded iframe and script elements for consent.<br><strong>Note</strong>: Leave this unchecked, if you want only processing when a known provider is detected.'),
      ];

      $form['consent_support'][static::CONF_CATEGORY_KEY] = [
        '#type' => 'select',
        '#title' => $category_id_title,
        '#options' => $category_id_options,
        '#default_value' => $bundle_config->get(static::CONF_CATEGORY_KEY),
        '#description' => $category_id_description,
      ];

      $form['consent_support'][static::CONF_CONTEXT] = [
        '#type' => 'select',
        '#title' => $context_title,
        '#options' => $consent_context_options,
        '#default_value' => $bundle_config->get(static::CONF_CONTEXT) ? $bundle_config->get(static::CONF_CONTEXT) : '',
        '#empty_value' => '',
        '#description' => $context_description,
      ];

      $permission = $this->t('Allow to override configuration for @entity_type', ['@entity_type' => $entity->getEntityType()->getLabel()]);
      $form['consent_support'][static::CONF_ALLOW_OVERRIDE] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Allow this configuration to be overridden for individual entities'),
        '#default_value' => $bundle_config->get(static::CONF_ALLOW_OVERRIDE),
        '#description' => $this->t('Allow users with the %permission permission to override this configuration for individual entities.', ['%permission' => $permission]),
      ];
    }

    // Entity config.
    else {

      /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
      $bundle_id = $entity->bundle();
      $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();

      // @todo When can this happen? Can this be removed?
      if (is_null($bundle_entity_type)) {
        $bundle_entity_type = $entity->getEntityType()->id();
        $bundle_id = NULL;
      }

      /** @var \Drupal\Core\Config\ImmutableConfig $bundle_config */
      $bundle_config = $this->configManager->loadConsentSupportImmutableConfig($bundle_entity_type, $bundle_id);

      // If the bundle does not allow config to be overridden.
      if (!$bundle_config->get(static::CONF_ALLOW_OVERRIDE)) {
        return;
      }

      // Get the label for the bundle.
      $bundle_label = $this->bundleInfo[$entity_type_id][$bundle_id]['label'] ?? $this->t('entity type');

      if ($bundle_config->get(static::CONF_FORCE_CATEGORY)) {
        $form['consent_support'][static::CONF_FORCE_CATEGORY] = [
          '#type' => 'inline_template',
          '#template' => '<div class="form-item"><div class="option">{{ title }}</div><p class="description">{{ description }}</p></div>',
          '#context' => [
            'title' => $this->t('<strong>Warning</strong>: "Force consent category by provider" is enabled.'),
            'description' => $this->t('When a known provider is detected, processing will be applied regardless of the options below.'),
          ],
        ];
      }

      /** @var string|null $enabled */
      $enabled = $entity->get(static::CONF_ENABLED)->value;
      if ($entity->isNew() || is_null($enabled)) {
        $enabled = $bundle_config->get(static::CONF_ENABLED);
      }
      $form['consent_support'][static::CONF_ENABLED] = [
        '#type' => 'checkbox',
        '#title' => $enable_title,
        '#default_value' => $enabled,
        '#description' => $this->t('Process embedded iframe and script elements for consent.'),
      ];

      // Add the 'bundle default' to the category key options.
      $bundle_category_key = $bundle_config->get(static::CONF_CATEGORY_KEY);
      $params = [
        '@bundle' => $bundle_label,
        '@config' => $category_id_options[$bundle_category_key],
      ];
      $category_id_options = [
        static::CONF_USE_DEFAULT => $this->t("Global '@bundle' configuration (@config)", $params),
      ] + $category_id_options;

      $form['consent_support'][static::CONF_CATEGORY_KEY] = [
        '#type' => 'select',
        '#title' => $category_id_title,
        '#options' => $category_id_options,
        '#default_value' => $entity->get('category_key')->value ? $entity->get('category_key')->value : static::CONF_USE_DEFAULT,
        '#description' => $category_id_description,
      ];

      // Already the default 'None' option as auto added for optional selection
      // list, because we need it if the default config is also 'None'.
      $bundle_consent_context = $bundle_config->get(static::CONF_CONTEXT);

      // Add the 'bundle default' to the consent context options.
      $consent_context_options[''] = $this->t('- None -');
      $params = [
        '@bundle' => $bundle_label,
        '@config' => $consent_context_options[$bundle_consent_context],
      ];
      $consent_context_options = [
        static::CONF_USE_DEFAULT => $this->t("Global '@bundle' configuration (@config)", $params),
      ] + $consent_context_options;

      /** @var string|null $context */
      $context = $entity->get(static::CONF_CONTEXT)->value;
      if ($entity->isNew() || is_null($context)) {
        $context = static::CONF_USE_DEFAULT;
      }
      $form['consent_support'][static::CONF_CONTEXT] = [
        '#type' => 'select',
        '#title' => $context_title,
        '#options' => $consent_context_options,
        '#default_value' => $context,
        '#empty_value' => '',
        '#description' => $context_description,
      ];
    }

    // Attach the Consent Support form to the main form, and add a custom
    // validation callback.
    $attach += $form;

    // If the implementing module provides a submit function for the bundle
    // form, we'll add it as a submit function for the attached form.
    try {

      $plugin = $this->entityPluginManager->createInstanceByEntityType($entity_type_id);
      $submit_handler_locations = $plugin->getFormSubmitHandlerAttachLocations();

      foreach ($submit_handler_locations as $location) {
        $array_ref = &$attach;
        if (is_array($location)) {
          foreach ($location as $subkey) {
            $array_ref = &$array_ref[$subkey];
          }
        }
        else {
          $array_ref = &$array_ref[$location];
        }

        $array_ref[] = '_consent_support_general_form_submit';
      }
    }
    catch (\Exception $exception) {
      $params = [
        'error' => $exception->getMessage(),
      ];
      $this->logger->error('addExtendedSupportOptionsToForm: {error}', $params);
    }
  }

}
