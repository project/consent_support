<?php

namespace Drupal\consent_support\Extender;

use Drupal\consent_support\Config\ConfigKeysInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface FormExtenderInterface.
 *
 * Defines entity forms extensions for Consent Support.
 */
interface FormExtenderInterface extends ConfigKeysInterface {

  /**
   * Helper form field (hidden).
   *
   * @var string
   */
  const HIDDEN_FIELD_ENTITY_TYPE = 'consent_support_entity_type';

  /**
   * Helper form field (hidden).
   *
   * @var string
   */
  const HIDDEN_FIELD_IS_BUNDLE = 'consent_support_is_bundle';

  /**
   * Form structure for the Consent Support configuration.
   *
   * This should be used by other modules that wish to implement the Consent
   * Support configurations in any form.
   *
   * @param array $attach
   *   The form that the Consent Support form should be attached to.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that we're adding the form to.
   */
  public function addExtendedSupportOptionsToEntityForm(array &$attach, EntityInterface $entity): void;

  /**
   * Handle general aspects of Consent Support form submission.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function handleFormSubmit(array $form, FormStateInterface $form_state): void;

}
