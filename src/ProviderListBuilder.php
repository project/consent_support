<?php

namespace Drupal\consent_support;

use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * List Builder for Provider entity.
 */
class ProviderListBuilder extends ConfigEntityListBuilder {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    protected CookieProInterface $cookiePro,
  ) {
    parent::__construct($entity_type, $storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('cookiepro_plus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {

    $header = [
      'id' => $this->t('ID'),
      'label' => $this->t('Label'),
      'category' => $this->t('Category'),
      'expression' => $this->t('Expression'),
    ];

    return array_merge($header, parent::buildHeader());
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {

    /** @var \Drupal\consent_support\Entity\ProviderInterface $entity */
    $category = $entity->getCategory();
    $labels = $this->cookiePro->getCategoryIdLabels();

    $row = [
      'id' => $entity->id(),
      'label' => $entity->label(),
      'category' => $labels[$category] ?? $category,
      'expression' => $entity->getExpression(),
    ];

    return array_merge($row, parent::buildRow($entity));
  }

}
