<?php

namespace Drupal\consent_support\Controller;

use Drupal\consent_support\NoConsentMessageInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NoConsentController.
 *
 * Builds the response for the consent_support.no_consent route.
 */
class NoConsentController extends ControllerBase {

  /**
   * CookieProNoConsentController constructor.
   *
   * @param \Drupal\Core\Render\BareHtmlPageRendererInterface $bareHtmlPageRenderer
   *   The bare page renderer service.
   * @param \Drupal\consent_support\NoConsentMessageInterface $noConsentMessage
   *   The CookiePro No Consent Message service.
   */
  public function __construct(
    protected BareHtmlPageRendererInterface $bareHtmlPageRenderer,
    protected NoConsentMessageInterface $noConsentMessage,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): NoConsentController {
    return new static(
      $container->get('bare_html_page_renderer'),
      $container->get('consent_support.no_consent_message')
    );
  }

  /**
   * Get No Consent page for given category.
   *
   * @param string $category_key
   *   The category ID key.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The rendered HTML response, ready to be sent.
   */
  public function getMessage(string $category_key): HtmlResponse {

    $content = $this->noConsentMessage->getMessageForIframe($category_key);

    // Render (bare) page with only our addition JS attached.
    $page_theme_property = 'consent_support_no_consent_page';
    $page_additions = [
      '#attached' => [
        'library' => [
          'consent_support/consent_support.cookiepro_no_consent',
        ],
      ],
    ];

    return $this->bareHtmlPageRenderer->renderBarePage($content, '', $page_theme_property, $page_additions);
  }

}
