<?php

namespace Drupal\consent_support\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class ConfigController.
 *
 * Builds the response for the consent_support.configuration route.
 */
class ConfigController extends ControllerBase {

  /**
   * Get config page content.
   *
   * @return array
   *   The rendered HTML response, ready to be sent.
   */
  public function getContent(): array {

    $params = [
      ':context_url' => Url::fromRoute('entity.consent_context.collection')->toString(),
      ':provider_url' => Url::fromRoute('entity.provider.collection')->toString(),
    ];

    return [
      '#type' => 'inline_template',
      '#template' => '<p>{{info}}</p>',
      '#context' => [
        'info' => $this->t('No general configuration for now, but be sure to check the <a href=":context_url" target="_blank">Consent Contexts</a> and <a href=":provider_url" target="_blank">Providers</a> and add your own.', $params),
      ],
    ];
  }

}
