<?php

namespace Drupal\consent_support\Config;

use Drupal\consent_support\Entity\Config;
use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Class ConfigManager.
 *
 * Manages Consent Support Config for entity types, bundles and entities.
 */
class ConfigManager implements ConfigManagerInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration factory service.
   */
  public function __construct(protected ConfigFactory $configFactory) {}

  /**
   * {@inheritdoc}
   */
  public function loadConsentSupportImmutableConfig(string $entity_type_id, string $entity_id = NULL): ?ImmutableConfig {

    $actual = $this->configFactory->get($this->generateExtendedSupportConfigFullId($entity_type_id, $entity_id, TRUE));
    if (!$actual->isNew()) {
      return $actual;
    }
    else {
      return $this->configFactory->get('consent_support.config.default');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadConsentSupportConfig(ConfigEntityInterface|FieldableEntityInterface $entity): ?ConfigInterface {

    // Get bundle config.
    $entity_type = $entity->getEntityType();
    $bundle_entity_type = $entity_type->getBundleEntityType() ?: $entity_type->id();
    $entity_id = $entity_type->getBundleEntityType() ? $entity->bundle() : NULL;
    $bundle_config = $this->loadConsentSupportImmutableConfig($bundle_entity_type, $entity_id);

    // Set defaults with bundle config.
    $category_key = $bundle_config->get(static::CONF_CATEGORY_KEY);
    $consent_context = $bundle_config->get(static::CONF_CONTEXT);
    $enabled = $bundle_config->get(static::CONF_ENABLED);
    $force_category = $bundle_config->get(static::CONF_FORCE_CATEGORY);

    if ($bundle_config->get(static::CONF_ALLOW_OVERRIDE) && $entity instanceof FieldableEntityInterface) {

      // Prepare entity config values.
      $entity_category_key = $entity->hasField(static::CONF_CATEGORY_KEY) ? $entity->get(static::CONF_CATEGORY_KEY)->value : NULL;
      if (!empty($entity_category_key) && ($entity_category_key !== static::CONF_USE_DEFAULT)) {
        $category_key = $entity_category_key;
      }

      $entity_consent_context = $entity->hasField(static::CONF_CONTEXT) ? $entity->get(static::CONF_CONTEXT)->value : NULL;
      if (!empty($entity_category_key) && $entity_consent_context !== static::CONF_USE_DEFAULT) {
        $consent_context = $entity_consent_context;
      }

      $entity_enabled = $entity->hasField(static::CONF_ENABLED) ? $entity->get(static::CONF_ENABLED)->value : NULL;
      if (!is_null($entity_enabled)) {
        $enabled = $entity_enabled;
      }
    }

    $config = Config::create();
    $config->setCategoryKey($category_key);
    $config->setContext($consent_context ?? '');
    $config->setEnabled($enabled);
    $config->setForceCategory($force_category);

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function saveConsentSupportConfig(array $config, string $entity_type_id, string $entity_id = NULL): void {

    // No config prefix needed when loading config entities directly.
    $config_id = $this->generateExtendedSupportConfigFullId($entity_type_id, $entity_id);

    $entity = Config::load($config_id);
    if ($entity === NULL) {
      $entity_array = ['id' => $config_id];
      $entity_array += $config;
      $entity = Config::create($entity_array);
    }
    else {
      foreach ($config as $key => $setting) {
        $entity->set($key, $setting);
      }
    }
    $entity->set('entity_type_id', $entity_type_id);
    $entity->set('entity_id', $entity_id);
    $entity->save();
  }

  /**
   * Generate a full ID based on entity type label and entity ID.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $entity_id
   *   The entity ID.
   * @param bool $config_prefix
   *   Add config prefix if TRUE.
   *
   * @return string
   *   The full ID appropriate for a ESConfig entity.
   */
  protected function generateExtendedSupportConfigFullId(string $entity_type_id, string $entity_id = NULL, bool $config_prefix = FALSE): string {

    if ($config_prefix) {
      $config_id = "consent_support.config.$entity_type_id";
    }
    else {
      $config_id = $entity_type_id;
    }

    if ($entity_id) {
      $config_id = "$config_id.$entity_id";
    }

    return $config_id;
  }

}
