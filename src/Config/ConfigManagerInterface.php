<?php

namespace Drupal\consent_support\Config;

use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Interface ESConfigManagerInterface.
 *
 * Defines the manager for Consent Support Config for entity types, bundles and
 * entities.
 */
interface ConfigManagerInterface extends ConfigKeysInterface {

  /**
   * Load Consent Support config, or defaults, for an entity or bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $entity_id
   *   The entity ID.
   *
   * @return \Drupal\Core\Config\ImmutableConfig|null
   *   The config object or NULL if it does not exist.
   */
  public function loadConsentSupportImmutableConfig(string $entity_type_id, string $entity_id = NULL): ?ImmutableConfig;

  /**
   * Load Consent Support config, or defaults, for an entity or bundle.
   *
   * In case of entities, the entity config is merged with bundle config.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface|\Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\consent_support\Entity\ConfigInterface|null
   *   The config object or NULL if it does not exist.
   */
  public function loadConsentSupportConfig(ConfigEntityInterface|FieldableEntityInterface $entity): ?ConfigInterface;

  /**
   * Save config for an entity or bundle.
   *
   * @param array $config
   *   The values for the ESConfig entity.
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $entity_id
   *   The entity ID.
   */
  public function saveConsentSupportConfig(array $config, string $entity_type_id, string $entity_id = NULL): void;

}
