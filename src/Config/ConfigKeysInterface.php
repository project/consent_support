<?php

namespace Drupal\consent_support\Config;

/**
 * Configuration keys, used in various classes.
 */
interface ConfigKeysInterface {

  /**
   * The 'allow override' config key.
   *
   * @var string
   */
  const CONF_ALLOW_OVERRIDE = 'allow_override';

  /**
   * The 'category id' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_KEY = 'category_key';

  /**
   * The 'context' config key.
   *
   * @var string
   */
  const CONF_CONTEXT = 'context';

  /**
   * The 'enabled' config key.
   *
   * @var string
   */
  const CONF_ENABLED = 'enabled';

  /**
   * The 'force_category' config key.
   *
   * @var string
   */
  const CONF_FORCE_CATEGORY = 'force_category';

  /**
   * Config value indicating the bundle config must be used.
   *
   * @var string
   */
  const CONF_USE_DEFAULT = 'bundle_default';

}
