CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Maintainers


INTRODUCTION
------------

This is a very basic solution to support [URL Embed](https://www.drupal.org/project/url_embed).  
Categories are always enforced, so make sure to add the necessary Providers for
the third-party content you expect.


REQUIREMENTS
------------

This module (currently) requires the following modules:

* [URL Embed](https://www.drupal.org/project/url_embed)


INSTALLATION
------------

Just enable the module.


CONFIGURATION
-------------

See the main module [README](./../../README.md) configuration section for more
information about Providers.


TROUBLESHOOTING
---------------

* For module issues, see the [module issue queue](https://www.drupal.org/project/issues/consent_support).


MAINTAINERS
-----------

* Andreas De Rijcke (andreasderijcke) - https://www.drupal.org/user/3260992

This project has been sponsored by:

* CALIBRATE: We are a full-service Drupal service provider based out of Europe.
  We help our clients with digital strategic planning, UX and UI design and
  testing, development, and digital marketing services. As a data driven
  strategic partner, we use facts, research and KPI's to achieve better results.
  Want to learn more about Calibrate? Visit www.calibrate.be.
