<?php

namespace Drupal\consent_support_url_embed\Plugin\Filter;

use Drupal\consent_support\Entity\Config;
use Drupal\consent_support\Processor\ProcessorInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\url_embed\Plugin\Filter\UrlEmbedFilter;
use Drupal\url_embed\UrlEmbedInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ESUrlEmbedFilter.
 *
 * Replaces the UrlEmbedFilter to provide Consent Support.
 */
class CsUrlEmbedFilter extends UrlEmbedFilter {

  /**
   * Constructs a UrlEmbedFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\url_embed\UrlEmbedInterface $url_embed
   *   The URL embed service.
   * @param \Drupal\consent_support\Processor\ProcessorInterface $processor
   *   The Consent Support processor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UrlEmbedInterface $url_embed, protected ProcessorInterface $processor) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $url_embed);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('url_embed'),
      $container->get('consent_support.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = parent::process($text, $langcode);

    $result->addCacheTags($this->processor->getCookiePro()->getConfig()->getCacheTags());
    $result->addCacheTags($this->processor->getCacheTags());

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function replaceNodeContent(\DOMNode $node, string $content): void {

    // Don't filter on admin pages, excluded paths or for whitelisted IPs.
    if ($this->processor->getCookiePro()->mustProcess()) {

      // Force processing by provider.
      $config = Config::create()->setForceCategory(TRUE);
      $this->processor->processHtmlAsString($content, $config);
    }

    parent::replaceNodeContent($node, $content);
  }

}
