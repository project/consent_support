CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Maintainers


INTRODUCTION
------------

This module adds Consent Support for Paragraphs.
You can set your processing defaults on each Paragraph type and add overrides at
entity level.

In addition to the default Paragraph widgets, we have also included support for

* [Paragraphs asymmetric translation widgets](https://www.drupal.org/project/paragraphs_asymmetric_translation_widgets), currently D9 only.
* [Paragraph Browser](https://www.drupal.org/project/paragraphs_browser), currently D9 only.
* [Paragraph Previewer](https://www.drupal.org/project/paragraphs_previewer), currently D9 only.

Processing of the Paragraph fields is limited to the different text field types,
and [Iframe](https://www.drupal.org/project/iframe) fields.

As the processing configuration is set per Paragraph (type), all fields that
contain content to block will be treated the same.


REQUIREMENTS
------------

This module (currently) requires the following modules:

 * [Paragraphs](https://www.drupal.org/project/paragraph)


INSTALLATION
------------

Just enable the module.


CONFIGURATION
-------------

On the Paragraph type entity form, you should see a 'Consent Support' section to
configure the default processing behaviour for Paragraph entities of this type.

See the main module [README](./../../README.md) configuration section for
explanation of the options.


TROUBLESHOOTING
---------------

* For module issues, see the [module issue queue](https://www.drupal.org/project/issues/consent_support).


MAINTAINERS
-----------

* Andreas De Rijcke (andreasderijcke) - https://www.drupal.org/user/3260992

This project has been sponsored by:

* CALIBRATE: We are a full-service Drupal service provider based out of Europe.
  We help our clients with digital strategic planning, UX and UI design and
  testing, development, and digital marketing services. As a data driven
  strategic partner, we use facts, research and KPI's to achieve better results.
  Want to learn more about Calibrate? Visit www.calibrate.be.
