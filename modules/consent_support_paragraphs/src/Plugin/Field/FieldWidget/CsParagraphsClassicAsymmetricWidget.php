<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\paragraphs_asymmetric_translation_widgets\Plugin\Field\FieldWidget\ParagraphsClassicAsymmetricWidget;

/**
 * Plugin implementation of the 'entity_reference_paragraphs' widget.
 *
 * No annotation, as we want to extend and replace the original, not add a new
 * widget.
 */
class CsParagraphsClassicAsymmetricWidget extends ParagraphsClassicAsymmetricWidget {

  use CsParagraphsWidgetTrait;

}
