<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\ParagraphsBrowserWidget;

/**
 * Plugin implementation of the 'paragraphs_browser' widget.
 *
 * No annotation, as we want to extend and replace the original, not add a new
 * widget.
 */
class CsParagraphsBrowserWidget extends ParagraphsBrowserWidget {

  use CsParagraphsWidgetTrait {
    CsParagraphsWidgetTrait::formElement as extendedSupportFormElement;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    return $this->extendedSupportFormElement($items, $delta, $element, $form, $form_state);
  }

}
