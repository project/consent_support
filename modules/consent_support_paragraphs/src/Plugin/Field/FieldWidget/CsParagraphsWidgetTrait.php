<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\consent_support\Extender\FormExtenderInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait ParagraphsWidgetTrait.
 *
 * Consent Support basis for Paragraphs Widgets.
 */
trait CsParagraphsWidgetTrait {

  /**
   * Constructs a ParagraphsWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   * @param \Drupal\consent_support\Extender\FormExtenderInterface $formExtender
   *   Consent Support form extender.
   */
  public function __construct(
    string $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityFieldManagerInterface $entity_field_manager,
    protected AccountProxyInterface $account,
    protected FormExtenderInterface $formExtender,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $entity_field_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('consent_support.form_extender'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Get widget state from parent.
    $widget_state = static::getWidgetState($element['#field_parents'], $this->fieldDefinition->getName(), $form_state);
    $delta_settings = $widget_state['paragraphs'][$delta];

    if ($delta_settings['mode'] === 'edit') {

      if ($this->account->hasPermission('consent support override paragraph')) {

        // Ajax calls (Paragraphs Previewer) might cause rerun, so make sure
        // we don't add our stuff twice.
        if (!isset($element['subform']['consent_support'])) {

          // Determine appropriate weight for our settings.
          $element_children = Element::children($element['subform'], TRUE);
          if (empty($element_children)) {
            $weight = 0;
          }
          else {
            $last = end($element_children);
            $weight = $element['subform'][$last]['#weight'] + 1;
          }

          $this->formExtender->addExtendedSupportOptionsToEntityForm($element['subform'], $delta_settings['entity']);
          $element['subform']['consent_support']['#weight'] = $weight;

          // Prevent the configuration to be moved to the parent entity's
          // advanced settings.
          unset($element['subform']['consent_support']['#group']);
        }
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $values = parent::massageFormValues($values, $form, $form_state);

    foreach ($values as &$item) {

      if (isset($item['subform']['consent_support'])) {

        /** @var \Drupal\paragraphs\ParagraphInterface $entity */
        $entity = &$item['entity'];

        // Remove the hidden helper fields.
        unset($item['subform']['consent_support'][FormExtenderInterface::HIDDEN_FIELD_ENTITY_TYPE]);
        unset($item['subform']['consent_support'][FormExtenderInterface::HIDDEN_FIELD_IS_BUNDLE]);

        foreach ($item['subform']['consent_support'] as $key => $value) {
          $entity->set($key, $value);
        }
      }
    }

    return $values;
  }

}
