<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\paragraphs\Plugin\Field\FieldWidget\InlineParagraphsWidget;

/**
 * Plugin implementation of the 'entity_reference_paragraphs' widget.
 *
 * No annotation, as we want to extend and replace the original, not add a new
 * widget.
 */
class CsInlineParagraphsWidget extends InlineParagraphsWidget {

  use CsParagraphsWidgetTrait;

}
