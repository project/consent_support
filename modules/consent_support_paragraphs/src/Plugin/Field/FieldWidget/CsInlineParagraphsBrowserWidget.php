<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\InlineParagraphsBrowserWidget;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\InlineParagraphsBrowserWidgetTrait;

/**
 * Plugin implementation of the 'entity_reference_paragraphs_browser' widget.
 *
 * No annotation, as we want to extend and replace the original, not add a new
 * widget.
 */
class CsInlineParagraphsBrowserWidget extends InlineParagraphsBrowserWidget {

  use CsParagraphsWidgetTrait {
    CsParagraphsWidgetTrait::formElement as extendedSupportFormElement;
  }

  use InlineParagraphsBrowserWidgetTrait {
    InlineParagraphsBrowserWidgetTrait::formElement as browserFormElement;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = $this->browserFormElement($items, $delta, $element, $form, $form_state);
    return $this->extendedSupportFormElement($items, $delta, $element, $form, $form_state);
  }

}
