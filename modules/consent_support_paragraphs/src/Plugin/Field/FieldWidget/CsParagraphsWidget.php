<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget;

/**
 * Plugin implementation of the 'entity_reference_revisions paragraphs' widget.
 *
 * No annotation, as we want to extend and replace the original, not add a new
 * widget.
 */
class CsParagraphsWidget extends ParagraphsWidget {

  use CsParagraphsWidgetTrait;

}
