<?php

namespace Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs_previewer\Plugin\Field\FieldWidget\ParagraphsPreviewerWidget;
use Drupal\paragraphs_previewer\Plugin\Field\FieldWidget\ParagraphsPreviewerWidgetTrait;

/**
 * Plugin implementation of the 'paragraphs_previewer' widget.
 *
 * No annotation, as we want to extend and replace the original, not add a new
 * widget.
 */
class CsParagraphsPreviewerWidget extends ParagraphsPreviewerWidget {

  use CsParagraphsWidgetTrait {
    CsParagraphsWidgetTrait::formElement as extendedSupportFormElement;
  }

  use ParagraphsPreviewerWidgetTrait {
    CsParagraphsWidgetTrait::formElement as previewerFormElement;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = $this->previewerFormElement($items, $delta, $element, $form, $form_state);
    return $this->extendedSupportFormElement($items, $delta, $element, $form, $form_state);
  }

}
