<?php

namespace Drupal\consent_support_paragraphs\Plugin\ConsentSupportPlugin;

use Drupal\consent_support\Plugin\ConsentSupportPluginBase;

/**
 * Implements Consent Support for Paragraphs Type.
 *
 * Required for the default Consent Support settings for the Paragraphs Type
 * and permissions for it.
 *
 * @ConsentSupportPlugin(
 *   id = "consent_support_paragraphs_type",
 *   label = @Translation("Paragraphs Type"),
 *   entityType = "paragraphs_type"
 * )
 */
class ParagraphType extends ConsentSupportPluginBase {}
