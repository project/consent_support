<?php

namespace Drupal\consent_support_paragraphs\Plugin\ConsentSupportPlugin;

use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\consent_support\Plugin\ConsentSupportPluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements Consent Support for Paragraph.
 *
 * @ConsentSupportPlugin(
 *   id = "consent_support_paragraph",
 *   label = @Translation("Paragraph"),
 *   entityType = "paragraph"
 * )
 */
class Paragraph extends ConsentSupportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void {
    $this->processor->processDisplay($build, $entity, $display, $config);
  }

}
