<?php

/**
 * @file
 * Module hooks.
 */

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Implements hook_entity_base_field_info().
 */
function consent_support_paragraphs_entity_base_field_info(EntityTypeInterface $entity_type): array {

  if ($entity_type->id() === 'paragraph') {

    /** @var \Drupal\consent_support\Extender\EntityExtenderInterface $extender */
    $extender = \Drupal::service('consent_support.entity_extender');
    return $extender->getExtendedSupportFields($entity_type->id());
  }

  return [];
}

/**
 * Implements hook_field_widget_info_alter().
 */
function consent_support_paragraphs_field_widget_info_alter(array &$info): void {

  $info['entity_reference_paragraphs']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsInlineParagraphsWidget';
  $info['paragraphs']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsParagraphsWidget';

  $module_handler = \Drupal::service('module_handler');

  // Support Paragraphs Asymmetric Translation.
  if ($module_handler->moduleExists('paragraphs_asymmetric_translation_widgets')) {
    $info['paragraphs_classic_asymmetric']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsParagraphsClassicAsymmetricWidget';
  }

  // Support Paragraphs Browser.
  if ($module_handler->moduleExists('paragraphs_browser')) {
    $info['entity_reference_paragraphs_browser']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsInlineParagraphsBrowserWidget';
    $info['paragraphs_browser']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsParagraphsBrowserWidget';
  }

  // Support Paragraphs Previewer.
  if ($module_handler->moduleExists('paragraphs_previewer')) {
    $info['entity_reference_paragraphs_previewer']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsInlineParagraphsPreviewerWidget';
    $info['paragraphs_previewer']['class'] = 'Drupal\consent_support_paragraphs\Plugin\Field\FieldWidget\CsParagraphsPreviewerWidget';
  }
}
