<?php

namespace Drupal\consent_support_block_content\Plugin\ConsentSupportPlugin;

use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\consent_support\Plugin\ConsentSupportPluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements Consent Support for Block Content.
 *
 * @ConsentSupportPlugin(
 *   id = "consent_support_block_content",
 *   label = @Translation("Block Content"),
 *   entityType = "block_content"
 * )
 */
class BlockContent extends ConsentSupportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void {
    $this->processor->processDisplay($build, $entity, $display, $config);
  }

}
