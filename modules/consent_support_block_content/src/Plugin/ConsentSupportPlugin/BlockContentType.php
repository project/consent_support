<?php

namespace Drupal\consent_support_block_content\Plugin\ConsentSupportPlugin;

use Drupal\consent_support\Plugin\ConsentSupportPluginBase;

/**
 * Implements Consent Support for Block Content Type.
 *
 * Required for the default Consent Support settings for the Block Content
 * Type and permissions for it.
 *
 * @ConsentSupportPlugin(
 *   id = "consent_support_block_content_type",
 *   label = @Translation("Block Content Type"),
 *   entityType = "block_content_type"
 * )
 */
class BlockContentType extends ConsentSupportPluginBase {}
