CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Maintainers


INTRODUCTION
------------

This is a very basic solution to support [Video Embed Field](https://www.drupal.org/project/video_embed_field).  
Categories are always enforced, so make sure to add the necessary Providers for
the third-party content you expect.

### Formatters

Both LazyLoad and Colorbox field formatter needed to be altered for CookiePro to
be able to do its magic.  
However, this comes at a (small) cost: the iframes are loaded as part of the
page and not delayed until the preview image is clicked, thereby undoing the
performance gain of both formatters. However, the iframe markup and 'no-consent'
page are cacheable.


REQUIREMENTS
------------

This module (currently) requires the following modules:

* [Video Embed Field](https://www.drupal.org/project/video_embed_field)


INSTALLATION
------------

Just enable the module.


CONFIGURATION
-------------

See the main module [README](./../../README.md) configuration section for more
information about Providers.


TROUBLESHOOTING
---------------

* For module issues, see the [module issue queue](https://www.drupal.org/project/issues/consent_support).


MAINTAINERS
-----------

* Andreas De Rijcke (andreasderijcke) - https://www.drupal.org/user/3260992

This project has been sponsored by:

* CALIBRATE: We are a full-service Drupal service provider based out of Europe.
  We help our clients with digital strategic planning, UX and UI design and
  testing, development, and digital marketing services. As a data driven
  strategic partner, we use facts, research and KPI's to achieve better results.
  Want to learn more about Calibrate? Visit www.calibrate.be.
