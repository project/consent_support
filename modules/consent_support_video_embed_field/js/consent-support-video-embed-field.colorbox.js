/**
 * @file
 * The cookiepro_video_embed_field colorbox integration.
 */

(function ($, Drupal, once) {
  /**
   * Drupal behaviors.
   *
   * @type {{attach: Drupal.behaviors.consent_support_video_embed_field_colorbox.attach}}
   */
  Drupal.behaviors.consent_support_video_embed_field_colorbox = {
    attach(context, settings) {
      once(
        'consent_support',
        '.video-embed-field-launch-modal',
        context,
      ).forEach(function (el) {
        $(el).click(function (event) {
          // Allow the thumbnail that launches the modal to link to other places
          // such as video URL, so if the modal is sidestepped things degrade
          // gracefully.
          event.preventDefault();
          $.colorbox(
            $.extend(settings.colorbox, {
              html: $(this).children('.data-video-embed-field-modal').html(),
            }),
          );
        });
      });
    },
  };
})(jQuery, Drupal, once);
