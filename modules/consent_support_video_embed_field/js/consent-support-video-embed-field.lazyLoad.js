/**
 * @file
 * The cookiepro_video_embed_field lazy loading videos.
 */

(function ($, Drupal, once) {
  /**
   * Drupal behaviors.
   *
   * @type {{attach: Drupal.behaviors.consent_support_video_embed_field_lazyLoad.attach}}
   */
  Drupal.behaviors.consent_support_video_embed_field_lazyLoad = {
    attach(context) {
      once('consent_support', '.video-embed-field-lazy', context).forEach(
        function (el) {
          $(el).click(function (event) {
            // Swap the lightweight image for the heavy JavaScript.
            event.preventDefault();
            const $this = $(this);
            $this.html($this.children('.data-video-embed-field-lazy').html());
          });
        },
      );
    },
  };
})(jQuery, Drupal, once);
