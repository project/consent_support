<?php

namespace Drupal\consent_support_video_embed_field\Plugin\Field\FieldFormatter;

use Drupal\consent_support\Processor\ProcessorInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\video_embed_field\Plugin\Field\FieldFormatter\LazyLoad;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the thumbnail field formatter.
 *
 * No annotation, as we want to extend the original, not add a new formatter.
 */
class CsLazyLoad extends LazyLoad {

  /**
   * Constructs a new instance of the plugin.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Field\FormatterInterface $thumbnail_formatter
   *   The field formatter for thumbnails.
   * @param \Drupal\Core\Field\FormatterInterface $video_formatter
   *   The field formatter for videos.
   * @param \Drupal\consent_support\Processor\ProcessorInterface $processor
   *   The Consent Support processor.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    RendererInterface $renderer,
    FormatterInterface $thumbnail_formatter,
    FormatterInterface $video_formatter,
    protected ProcessorInterface $processor,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $renderer, $thumbnail_formatter, $video_formatter);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $formatter_manager = $container->get('plugin.manager.field.formatter');
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
      $formatter_manager->createInstance('video_embed_field_thumbnail', $configuration),
      $formatter_manager->createInstance('video_embed_field_video', $configuration),
      $container->get('consent_support.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = parent::viewElements($items, $langcode);

    $cookiePro = $this->processor->getCookiePro();
    $cookiePro->addConfigCacheTags($element);

    if ($cookiePro->mustProcess()) {

      foreach ($items as $delta => $item) {

        // Extract iframe data.
        $modal_data = $element[$delta]['#attributes']['data-video-embed-field-lazy'];
        unset($element[$delta]['#attributes']['data-video-embed-field-lazy']);

        // Extract existing children (image preview).
        $children = $element[$delta]['children'];
        $element[$delta]['children'] = [];

        // Add modal data in invisible container, so CookiePro script can do its
        // work.
        $element[$delta]['children'][] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['data-video-embed-field-lazy'],
            'style' => 'display:none',
          ],
          'children' => [
            '#markup' => $modal_data,
            '#allowed_tags' => ['div', 'iframe'],
          ],
        ];
        // Re-add original children.
        $element[$delta]['children'][] = $children;

        // Replace library.
        foreach ($element[$delta]['#attached']['library'] as $index => $name) {
          if ($name == 'video_embed_field/lazy-load') {
            $element[$delta]['#attached']['library'][$index] = 'consent_support_video_embed_field/lazy-load';
          }
        }
      }
    }

    return $element;
  }

}
