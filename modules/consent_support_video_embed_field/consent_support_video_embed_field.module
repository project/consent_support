<?php

/**
 * @file
 * Module hooks.
 */

use Drupal\consent_support\Entity\ConsentContextInterface;

/**
 * Implements hook_field_formatter_info_alter().
 */
function consent_support_video_embed_field_field_formatter_info_alter(array &$info): void {

  if (isset($info['video_embed_field_colorbox'])) {
    $info['video_embed_field_colorbox']['class'] = '\Drupal\consent_support_video_embed_field\Plugin\Field\FieldFormatter\CsColorbox';
  }

  if (isset($info['video_embed_field_lazyload'])) {
    $info['video_embed_field_lazyload']['class'] = '\Drupal\consent_support_video_embed_field\Plugin\Field\FieldFormatter\CsLazyLoad';
  }
}

/**
 * Implements hook_preprocess_video_embed_iframe().
 */
function consent_support_video_embed_field_preprocess_video_embed_iframe(array &$variables): void {

  /** @var \Drupal\consent_support\Processor\ProcessorInterface $processor */
  $processor = Drupal::service('consent_support.processor');
  $cookiePro = $processor->getCookiePro();
  $cookiePro->addConfigCacheTags($variables);

  if ($cookiePro->mustProcess()) {

    switch ($variables['theme_hook_original']) {

      case 'video_embed_iframe__youtube':
      case 'video_embed_iframe__youtube_playlist':

        $category_key = $processor->getCategoryKeyByProvider('youtube');
        break;

      case 'video_embed_iframe__vimeo':
        $category_key = $processor->getCategoryKeyByProvider('vimeo');
        break;

      default:
        return;
    }

    $processor->addCacheTags($variables);

    // Manipulate the iframe as required: rebuild full url.
    /** @var \Drupal\Core\Template\Attribute $attributes */
    $attributes = &$variables['attributes'];
    $attributes->addClass($processor->getClassName($category_key));

    $src = $variables['url'];
    unset($variables['url']);

    if (!empty($variables['query'])) {
      $src .= "?" . http_build_query($variables['query'], '', '&');
      unset($variables['query']);
    }

    if (!empty($variables['fragment'])) {
      $src .= "#{$variables['fragment']}";
      unset($variables['fragment']);
    }

    $attributes->setAttribute('data-src', $src);
    $attributes->setAttribute('src', $processor->getNoConsentPageUrl($category_key, ConsentContextInterface::CONTEXT_VIDEO));
  }
}
