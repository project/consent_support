CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

This module adds Consent Support for Media.
You can set your processing defaults on each Media type and add overrides at
entity level.

Processing is limited to the first item of string field types, as Media does not
support multiple URL’s per Media entity.


REQUIREMENTS
------------

This module (currently) requires the following modules:

 * Media, Drupal core


INSTALLATION
------------

Just enable the module.


CONFIGURATION
-------------

On the Media type entity form, you should see at the bottom a 'Consent Support'
tab to configure the default processing behaviour for Media entities of this
type.

See the main module [README](./../../README.md) configuration section for
explanation of the options.


TROUBLESHOOTING
---------------

 * For module issues, see the [module issue queue](https://www.drupal.org/project/issues/consent_support).


MAINTAINERS
-----------

 * Andreas De Rijcke (andreasderijcke) - https://www.drupal.org/user/3260992

This project has been sponsored by:

 * CALIBRATE: We are a full-service Drupal service provider based out of Europe.
  We help our clients with digital strategic planning, UX and UI design and
  testing, development, and digital marketing services. As a data driven
  strategic partner, we use facts, research and KPI's to achieve better results.
  Want to learn more about Calibrate? Visit www.calibrate.be.
