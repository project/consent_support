<?php

namespace Drupal\consent_support_media\Plugin\ConsentSupportPlugin;

use Drupal\consent_support\Plugin\ConsentSupportPluginBase;

/**
 * Implements Consent Support for Media Type.
 *
 * Required for the default Consent Support settings for the Media Type and
 * permissions for it.
 *
 * @ConsentSupportPlugin(
 *   id = "consent_support_media_type",
 *   label = @Translation("Media Type"),
 *   entityType = "media_type"
 * )
 */
class MediaType extends ConsentSupportPluginBase {}
