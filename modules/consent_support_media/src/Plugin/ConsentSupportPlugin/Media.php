<?php

namespace Drupal\consent_support_media\Plugin\ConsentSupportPlugin;

use Drupal\consent_support\ConsentCategoriesConstantsInterface;
use Drupal\consent_support\Entity\ConfigInterface;
use Drupal\consent_support\Plugin\ConsentSupportPluginBase;
use Drupal\consent_support\Processor\ProcessorInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\media\OEmbed\ProviderException;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\OEmbed\UrlResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Consent Support for Media.
 *
 * @ConsentSupportPlugin(
 *   id = "consent_support_media",
 *   label = @Translation("Media"),
 *   entityType = "media"
 * )
 */
class Media extends ConsentSupportPluginBase {

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\consent_support\Processor\ProcessorInterface $processor
   *   The Consent Support processor.
   * @param \Drupal\media\OEmbed\UrlResolverInterface $urlResolver
   *   The Media OEmbed Url Resolver service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ProcessorInterface $processor, protected UrlResolverInterface $urlResolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $processor);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Media {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('consent_support.processor'),
      $container->get('media.oembed.url_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processDisplay(array &$build, ContentEntityInterface $entity, EntityViewDisplayInterface $display, ConfigInterface $config): void {

    $this->processor->getCookiePro()->addConfigCacheTags($build);

    // Default category and class name we'll need to add.
    $category_key = $config->getCategoryKey();
    $class_name = $this->processor->getClassName($category_key);

    $fields = $entity->getFieldDefinitions();
    // Skip our own additional fields.
    $fields = array_diff_key($fields, array_flip($this->processor->getFieldNamesToIgnore()));

    $supported_types = [
      'string',
      'string_long',
    ];

    foreach ($fields as $field_name => $field) {

      // Find the field containing the URL. Any string field in the build array
      // is a potential target.
      if (in_array($field->getType(), $supported_types) && isset($build[$field_name])) {

        $field_items = $entity->get($field_name);
        /** @var \Drupal\Core\Field\FieldItemBase $item */
        foreach ($field_items as $index => $item) {

          // Field data is missing, probably because it couldn't be retrieved
          // from the source.
          if (!isset($build[$field_name][$index])) {
            continue;
          }

          try {
            // Test if Media can resolve to valid provider.
            $provider = $this->urlResolver->getProviderByUrl($item->get('value')->getValue());

            $this->processor->getCookiePro()->addConfigCacheTags($build[$field_name][$index]);
            $attributes = &$build[$field_name][$index]['#attributes'];
            if (isset($attributes['src'])) {

              $category_key_final = $category_key;
              $class_name_final = $class_name;

              // Force category if required.
              if ($config->getForceCategory()) {

                $category_key_forced = $this->processor->getCategoryKeyByProvider($provider->getName());
                if ($category_key_forced) {

                  $category_key_final = $category_key_forced;
                  $class_name_final = $this->processor->getClassName($category_key_forced);

                  // Category depends on Providers.
                  $this->processor->addCacheTags($build[$field_name][$index]);
                }
              }

              // Nothing to block, continue to the next element.
              if (!$config->getEnabled() ||
                empty($category_key_final) ||
                $category_key_final === ConsentCategoriesConstantsInterface::CONF_CATEGORY_STRICTLY_NECESSARY
              ) {
                continue;
              }

              $attributes['data-src'] = $build[$field_name][$index]['#attributes']['src'];
              $attributes['src'] = $this->processor->getNoConsentPageUrl($category_key_final, $config->getContext());
              $attributes['class'][] = $class_name_final;
            }
          }
          catch (ProviderException $exception) {
            // This exception should not bubble until here. Let it fail
            // silently for now.
            continue;
          }
          catch (ResourceException $exception) {
            // No valid provider found, assume no video url.
            continue;
          }
        }
      }
    }
  }

}
