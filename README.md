CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How it works
 * Roadmap
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

This module is a 'spin-off' from [CookiePro Plus](https://www.drupal.org/project/cookiepro_plus)
to support content blocking in a way that requires as minimal impact and/or
effort for content editors.

The initial goal of this module was twofold:

    1. Making the solution as less intrusive and foolproof as possible for
       content editors.

    2. Replacing blocked content with a 'no-consent message', or adding in case
       of scripts, which also contains link to open the CookiePro preferences
       center.

As CookiePro defines ways on how to handle [iframes](https://my.onetrust.com/s/article/UUID-3dc06342-ff6b-3578-5024-6d7c05d03283?topicId=0TO1Q000000ssbQWAQ)
and [scripts](https://my.onetrust.com/s/article/UUID-518074a1-a6da-81c3-be52-bae7685d9c94?topicId=0TO1Q000000ssJBWAY#idm45260969021504)
that are part of the content and cannot be handled by their Auto-Blocking™
mechanism. This these solutions are implemented in a generic way, which can be
expanded upon to handle specific entity types or fields.

The processing and alteration of the content happens at render time, so it does
not affect the actual content in the database.

Overtime, the module expanded with additions like:
 
 * 'No-consent messages' using templates.
 * A few submodules to cover common use cases.
 * In case of entities to be processed (Media, Paragraphs):
    * Default processing behavior is stored on the entity type.
    * Override at entity level is possible.
    * Permissions are present to allow or deny override at entity level to the
      desired User roles.
 * Consent Context (config): affects template suggestions for the 'no-consent 
   message' template. Which Consent Context is applied depends on the case.  
   In case of entities, it can be configured and overriden at entity level.
 * Provider (config): Similar to oEmbed providers, to tie provider domain(s) to
   a consent category using regex. This is used to enforce the proper consent
   category during processing.

Overall, this offers a lot of freedom on when to block content and how to
present it, while maintaining a certain degree of consistency.

The included submodules currently cover support for:

 * __Block Content aka Custom Block__ (Drupal core)
 * __Media__ (Drupal core)
 * [__Paragraphs__](https://www.drupal.org/project/paragraphs)
 * [__URL Embed__](https://www.drupal.org/project/url_embed)
 * [__Video Embed Field__](https://www.drupal.org/project/video_embed_field)


REQUIREMENTS
------------

This module (currently) requires the following modules:

 * [CookiePro Plus](https://www.drupal.org/project/cookiepro)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/node/1897420) for further
  information.


CONFIGURATION
-------------

Upon installation, some Consent Contexts and Providers will be installed.
Manage them using the module configuration page.

 * __Consent Contexts__: A Consent Context adds template suggestions for the
   'no-consent messages'. For entities, the default Consent Context can be
   configured at the entity type.  
   The module comes with Consent Contexts 'Video' and 'Script' and their
   corresponding templates.
 * __Provider__: A Provider is used during processing, either when the
   'Force consent category by provider' option is active for a given entity or a
   hard-coded solution is implemented.  
   A Provider contains an 'Expression' (regex), to find iframes and scripts to
   process, and a 'Category', being the (cookie) consent category to apply.  
   As this is tied to content, depending on the setup, a separate permission has 
   been added to allow  managing of Providers without access to the entire module
   config. However, allow with care and use [config_ignore](https://drupal.org/project/config_ignore)
   to prevent data loss.

When enabling one of the submodules that add support to an entity type,
following options will appear at the entity type edit form:

 * __Force consent category by provider__: this option overrules all others.
 * __Enable processing__: Enabling will cause iframes and scripts in the entity
   content to be 'blocked' and/or replaced with a 'no-consent message'.
 * __Category__: The category to apply. Allows CookiePro to unblock the content
   when the appropriate consent is given;
 * __Consent context__: Basically, choose the 'no-consent message' variation to
   use.
 * __Allow this configuration to be overridden for individual entities__: Only
   for entity types.  
   If enabled, Users with the proper permissions will be able
   to override the defaults on a single entity. Allowing this is discouraged,
   but might be required in some cases.

On entities of supported type, depending on if __Allow this configuration to be
overridden for individual entities__ is enabled and the user has the proper
permissions, a similar form will appear.
 
Last but not least, check the permissions page for 'Consent Support' permissions
you might want to grant to certain roles.


HOW IT WORKS
------------

The main goal of this module is to provide a framework to:

 * Rewrite iframe render arrays or iframe markup in a string so CookiePro can
   unblock its real content when the appropriate consent is given, and load
   a 'no-consent message' by default in the iframe.

 * Rewrite scripts in strings so CookiePro can unblock and execute the script
   when the appropriate consent is given, and inject a 'no-consent message'.

All of this is implemented in the service [Processor](./src/Processor.php).

As context is important and sources of the content to process can vary, from
plain text to modules offering certain field types, additional pieces are
available to add as much flexibility as possible.

 * [Preprocessor](./src/Preprocessor.php): determines if processing is
   required on given entity, using hook_entity_view_alter().
   If processing is required, it will call the plugin providing the support for
   given entity.
 * The plugin implementation of EntityPlugin for given entity, should do
   whatever manipulations can and/or need to be done.
   [ConsentSupportPluginBase](./src/Plugin/ConsentSupportPluginBase.php) is 
   available to make this easier.
 * The plugin implementation of EntityPlugin is encouraged to make use of the
   [Processor](./src/Processor.php) if possible.


### Adding support to entities/entity types

Implementing [ConsentSupportPluginBase](./src/Plugin/ConsentSupportPluginBase.php)
for a given entity (type) will add the necessary fields to the entity type
configuration form. And, if override is allowed at entity level, also to the
entity form.

 * The configuration on entity type is stored in config.
 * The configuration on an entity is part of the entity, using additional base
   fields.

Use hook_entity_base_field_info() to call the [EntityExtender](./src/Extender/EntityExtender.php)
service and get the additional base fields installed.

The [FormExtender](./src/Extender/FormExtender.php) will add the necessary form
fields to the entity (type) form, it the user has the proper permissions.

The submodule [Consent Support - Media](./modules/consent_support_media/README.md)
is perfect example for this.


### Adding support to field widgets.

We don't have a case that requires support configuration a field type level, but
we do have the combination of entity and field widget to support Paragraphs
containing text (Drupal core) and [Iframe](https://www.drupal.org/project/iframe)
fields.

As paragraphs don't have edit pages, we've attached the configuration at entity
level by overriding the ParagraphsWidget and have the [FormExtender](./src/Extender/FormExtender.php)
service attach the necessary fields.

See the submodule [Consent Support - Paragraphs](./modules/consent_support_paragraphs/README.md).


### Adding support to other cases

See our other submodules for inspiration on how to add support and process
other kinds of content.

If you have a generic case that could be covered by this module, let us know in
the [module issue queue](https://www.drupal.org/project/issues/consent_support).


ROADMAP
-------

As many cookie banner/consent tools use similar approaches to 'unblock' iframes
and scripts, the goal for version 2 is to reverse dependencies with
[CookiePro Plus](https://www.drupal.org/project/cookiepro) and define a
'processing' plugin for cookie banner/consent tools modules to implement.


TROUBLESHOOTING
---------------

* For module issues, see the [module issue queue](https://www.drupal.org/project/issues/consent_support).


MAINTAINERS
-----------

 * Andreas De Rijcke (andreasderijcke) - https://www.drupal.org/user/3260992

This project has been sponsored by:

 * CALIBRATE: We are a full-service Drupal service provider based out of Europe.
  We help our clients with digital strategic planning, UX and UI design and
  testing, development, and digital marketing services. As a data driven
  strategic partner, we use facts, research and KPI's to achieve better results.
  Want to learn more about Calibrate? Visit www.calibrate.be.
