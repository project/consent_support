/**
 * @file
 * consent-support-cookiepro-no-consent.js
 */

(function (Drupal) {
  /**
   * Drupal behaviors.
   *
   * @type {{attach: Drupal.consent_support_cookiepro_no_consent.attach}}
   */
  Drupal.behaviors.consent_support_cookiepro_no_consent = {
    attach(context) {
      context
        .querySelectorAll('.ot-sdk-show-settings')
        .forEach(function (value) {
          value.addEventListener('click', function () {
            parent.consent_support.cookiepro.showInfoDisplay();
          });
        });
    },
  };
})(Drupal);
